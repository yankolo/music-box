# MusicBox

MusicBox is a online music store.

## Environment Requirements
- [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Payara 5.184](https://www.payara.fish/software/downloads/)
- [MySQL 8](https://dev.mysql.com/downloads/mysql/8.0.html)
- [Maven 3.1+](https://maven.apache.org/download.cgi?Preferred=ftp://mirror.reverse.net/pub/apache/)

## How to run the project?
Make sure you have all the environment requirements for your system. Most importantly, you need to have a new version of Maven (at least 3.1), because some maven plugins depend on it. **NetBeans 8 comes with a lower version, so be sure to update NetBean's builtin maven!**

Next, you need to build the database. 
Run the following script:
 * **src/test/resources/scripts/create_database.sql** (with root privileges)
 * **src/test/resources/scripts/create_tables.sql**

Add the appropriate resources to Payara. If you're using Netbeans, the IDE will automatically detect the glassfish-resources.xml file in the src/main/setup and add the resources to the server when you deploy. 

If you're not using NetBeans, you can add the glassfish-resources.xml file manually through the admin panel. Alternatively, you can run the following commands

```sh
$ cd <payara installation folder>/bin
$ asadmin start-domain domain1
$ asadmin add-resources <project path>/src/main/setup/glassfish-resources.xml
```

After you have setup everything, compile the project with maven and deploy it to your local Payara server.

(Please note that there might be some bugs when running the project with IE11)
## Login Information

Some accounts are already created for testing purposes

### Administrator

* Email: PatriciaMBachman@rhyta.com
* Password: test123

### Normal User

* Email: GinaCHenson@dayrep.com
* Password: test123

## Development Guidelines
> Less discussing, More working

## Useful Links
| Name | Link |
| ------ | ------ |
| Google Drive | https://drive.google.com/drive/folders/1nmjnakkzIjlXyz7rcYfPMKTvvNkuA03A?usp=sharing |
| Vertabelo | https://my.vertabelo.com/doc/4EhCdAFHL1BdbOIh8LWAdooFE63G9eIv |
| Slack | https://cs3dawson2018.slack.com |


## Credits
Yanik Kolomatski, Danny Manzano-Tates, Lara Mezirovsky, Dimitar Benev

This project is done for the *420-625-DW Java Server Side* course in Dawson College, Montreal, Canada.
