/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Danny
 */
public class SeleniumTest {

    private WebDriver driver;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }

    @Test
    public void testSearch() {
        driver.get("http://localhost:8080/music-box/home.xhtml");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);

        // Wait for the page to load, timeout after 10 seconds
        wait.until(ExpectedConditions.titleIs("Music Box"));

        WebElement searchElement = driver.findElement(By.id("searchForm:searchTerm"));
        
        WebElement submitElement = driver.findElement(By.id("searchForm:searchSubmit"));
        
        searchElement.sendKeys("otis");
        
        submitElement.click();
        
        wait.until(ExpectedConditions.urlContains("catalog.xhtml"));
        
        ExpectedConditions.numberOfElementsToBe(By.id("units"), 4);
    }
    
    @Test
    public void testLogin(){
        driver.get("http://localhost:8080/music-box/storefront/pages/public/login.xhtml");
        
        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);

        // Wait for the page to load, timeout after 10 seconds
        wait.until(ExpectedConditions.titleIs("Music Box"));
        
        WebElement emailInput = driver.findElement(By.id("loginForm:email"));
        
        WebElement pwdInput = driver.findElement(By.id("loginForm:password"));
        
        WebElement submit = driver.findElement(By.id("loginForm:loginSubmit"));
        
        emailInput.sendKeys("GinaCHenson@dayrep.com");
        
        pwdInput.sendKeys("test123");
        
        submit.click();
        
        ExpectedConditions.urlContains("home.xhtml");
        
    }

    @After
    public void teardownTest() {
        driver.quit();
    }

}
