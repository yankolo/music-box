
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquillian;

import com.javaproject.musicbox.entities.Album;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Track;
import com.javaproject.musicbox.enums.FilterType;
import com.javaproject.musicbox.enums.SearchFilter;
import com.javaproject.musicbox.filters.AuthorizationFilter;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.ItemService;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.Cleanup;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.arquillian.persistence.TestExecutionPhase;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Danny
 */
@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class ItemServiceTest {
    @Inject
    private ItemService itemService;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }
    
    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */

    @Test
    public void injectService_testIsCorrectlySetup_serviceInjected() {
        Assert.assertNotNull(itemService);
    }
    
    @Test 
    public void getAllAlbums_amountFetch_64(){
        Assert.assertEquals(64, itemService.getAllAlbums().size());
    }
    
    @Test 
    public void findLastest_latestItem_cardiB(){
        List<Item> list = itemService.findLatest(3);
        
        Assert.assertEquals(3, list.size());
        
        Assert.assertEquals("Invasion of Privacy", ((Album)list.get(0)).getName());
    }
    
    @Test 
    public void findSpecials_size_empty(){
        List<Item> list = itemService.findSpecials(3);
        
        Assert.assertEquals(0, list.size());
    }
    
    @Test
    public void findByCriteria_otisListSizeTrackAlbum_4(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.ARTIST);
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        List<Item> list = itemService.findByCriteria("otis", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(4, list.size());
    }
    
    @Test
    public void findByCriteria_otisListSizeTrack_2(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.ARTIST);
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        List<Item> list = itemService.findByCriteria("otis", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(2, list.size());
    }
    
    @Test
    public void findByCriteria_otisListSizeAlbum_2(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.ARTIST);
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        List<Item> list = itemService.findByCriteria("otis", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(2, list.size());
    }
    
    @Test
    public void findByCriteria_otisListSizeNoArtist_0(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        List<Item> list = itemService.findByCriteria("otis", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(0, list.size());
    }
    
    @Test
    public void findByCriteria_columbiaLabel_3(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.LABEL);
        
        List<Item> list = itemService.findByCriteria("columbia", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(3, list.size());
    }
    
    @Test
    public void findByCriteria_classicalGenre_2(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.GENRE);
        
        List<Item> list = itemService.findByCriteria("classical", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(2, list.size());
    }
    
    @Test
    public void findByCriteria_HardToHandleTitle_1(){
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.TITLE);
        
        List<Item> list = itemService.findByCriteria("Hard To Handle", filterTypeList, searchFilterList, null, null);
        
        Assert.assertEquals(1, list.size());
    }
    
    @Test
    public void findByCriteria_byDates_165() throws ParseException{
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.DATES);
        searchFilterList.add(SearchFilter.ARTIST);
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        
        
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        
        
        Date from = dateformat.parse("2019-02-01");
        Date to = dateformat.parse("2019-04-01");
                
        
        List<Item> list = itemService.findByCriteria("", filterTypeList, searchFilterList, from, to);
        
        Assert.assertEquals(165, list.size());
    }
    
    @Test
    public void findByCriteria_byDates_0() throws ParseException{
        List<FilterType> filterTypeList = new ArrayList<>();
        filterTypeList.add(FilterType.TRACK);
        filterTypeList.add(FilterType.ALBUM);
        
        List<SearchFilter> searchFilterList = new ArrayList<>();
        searchFilterList.add(SearchFilter.DATES);
        searchFilterList.add(SearchFilter.ARTIST);
        searchFilterList.add(SearchFilter.LABEL);
        searchFilterList.add(SearchFilter.GENRE);
        searchFilterList.add(SearchFilter.TITLE);
        
        
        
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        
        
        Date from = dateformat.parse("2018-02-01");
        Date to = dateformat.parse("2018-04-01");
                
        
        List<Item> list = itemService.findByCriteria("", filterTypeList, searchFilterList, from, to);
        
        Assert.assertEquals(0, list.size());
    }
    
    @Test
    public void injectService_testIsCorrectlySetup_ItemserviceInjected() {
        Assert.assertNotNull(itemService);
    }

    @Test
    public void findAlbums_testIsCorrectlySetup_notNullAlbums() {
        Assert.assertNotNull(itemService.getAllAlbums());
    }

    @Test
    public void similarAlbumByGenre_hasAtLeastOneSimilarGenre_ThreeSimialrAlbumByGenre() {
        Assert.assertEquals(3, itemService.getAllItemsWithSameGenre("Hip-Hop", 1, "album").size());
    }

    @Test
    public void similarAlbumByGenre_hasAtLeastOneSimilarGenre_TheAlbumsGenreIsHipHop() {
        //instead of going one by one, I am making a loop that will test if the retreived album's genre is indeed hip-hop
        boolean validGenre = true;
        for (Item album : itemService.getAllItemsWithSameGenre("Hip-Hop", 1, "album")) {
            if ("Hip-Hop".equals(album.getCategoryList().get(0).toString())) {
                validGenre = false;
            }
        }
        Assert.assertTrue(validGenre); //must remain true for the test to pass
    }

    @Test
    public void similarTrackByGenre_hasAtLeastOneSimilarGenre_ThreeSimialrTracksByGenre() {
        Assert.assertEquals(3, itemService.getAllItemsWithSameGenre("Hip-Hop", 143, "track").size());
    }

    @Test
    public void similarTrackByGenre_hasAtLeastOneSimilarGenre_TheTrackGenreIsHipHop() {
        //instead of going one by one, I am making a loop that will test if the retreived tracks's genre is indeed hip-hop
        boolean validGenre = true;
        for (Item track : itemService.getAllItemsWithSameGenre("Hip-Hop", 143, "track")) {
            if ("Hip-Hop".equals(track.getCategoryList().get(0).toString())) {
                validGenre = false;
            }
        }
        Assert.assertTrue(validGenre); //must remain true for the test to pass
    }
}