/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arquillian;

import com.javaproject.musicbox.entities.Artist;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.filters.AuthorizationFilter;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.ArtistService;
import com.javaproject.musicbox.services.CategoryService;
import java.io.File;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.Cleanup;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.arquillian.persistence.TestExecutionPhase;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Danny
 */
@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class ArtistServiceTest {
    @Inject
    private ArtistService artistService;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }
    
    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */

    @Test
    public void injectService_testIsCorrectlySetup_serviceInjected() {
        Assert.assertNotNull(artistService);
    }
    
    @Test
    public void createArtist_johnnyArtist_created(){
        artistService.createArtist("Johnny Cash");
        
        Artist artist = artistService.findById(75);
        
        Assert.assertEquals("Johnny Cash", artist.getName());
    }
}
