package arquillian;

import com.javaproject.musicbox.managedbeans.storefront.ShoppingCartBean;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class ShoppingCartBeanTest {

    @Inject
    private ShoppingCartBean shoppingCartBean;

    @Deployment
    public static JavaArchive createDeployment()
    {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */
    @Test
    public void injectBean_testIsCorrectlySetup_beanInjected() {
        Assert.assertNotNull(shoppingCartBean);
    }
}
