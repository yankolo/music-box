package arquillian;

import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.eventsdata.LoginEventData;
import com.javaproject.musicbox.eventsdata.RegistrationEventData;
import com.javaproject.musicbox.filters.AuthorizationFilter;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.CategoryService;
import com.javaproject.musicbox.services.OrderService;
import com.javaproject.musicbox.services.UserService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.arquillian.persistence.*;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Date;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class OrderServiceTest {

    @Inject
    private OrderService orderService;

    @Inject
    private UserService userService;

    @Deployment
    public static JavaArchive createDeployment()
    {
        return ShrinkWrap.create(JavaArchive.class)
                .addClasses(AbstractService.class, OrderService.class, UserService.class)
                .addClass(CategoryService.class)
                .addPackage("com.javaproject.musicbox.eventsdata")
                .addPackage("com.javaproject.musicbox.entities")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */

    @Test
    public void injectService_testIsCorrectlySetup_serviceInjected() {
        Assert.assertNotNull(orderService);
    }

    @Test
    public void create_validOrder_OrderCreated() {
        Order expectedOrder = generateTestOrder();

        orderService.create(expectedOrder);
        Order actualOrder = orderService.findById(23);

        Assert.assertEquals(expectedOrder, actualOrder);
    }

    private Order generateTestOrder() {
        Order order = new Order();
        order.setUser(userService.findById(22));
        order.setSubtotal(10.00);
        order.setTaxes(1.50);
        order.setGrandTotal(11.50);
        order.setTimestamp(new Date());

        return order;
    }
}
