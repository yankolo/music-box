package arquillian;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.UserService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class UserServiceTest {
    @Inject
    private UserService userService;

    @Deployment
    public static JavaArchive createDeployment(){
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void registerUser_testIsUserCorrectlyRegister_correctlyRegistered() throws InvalidKeySpecException, NoSuchAlgorithmException {
        User user = new User();
        user.setEmail("test@gmail.com");
        user.setPassword("123");
        user.setAddress1("123 sherbrooke");
        user.setPostalCode("h3z1a4");
        user.setProvince("QC");
        user.setHomePhone("514");
        user.setFirstName("Dimitar");
        user.setLastName("Benev");
        user.setCity("Montreal");

        userService.registerUser(user);
        User newUser = userService.findById(24);
        Assert.assertEquals(user, newUser);
    }

    @Test
    public void checkEmailUsed_testIsUsedEmailCorrectlyDetected_correctlyFound(){
        Assert.assertTrue(userService.checkEmailUsed("test2@gmail.com"));
    }

    @Test
    public void setLastVisitedCategory_testIsLastVisitedCategoryCorrectlySet_correctlySet(){
        User user = userService.findById(3);
        userService.setLastVisitedCategory(user, "Rock");
        Assert.assertEquals("Rock", userService.findById(3).getLastVisitedCategory().getName());
    }

    @Test
    public void getTotalUserPurchases_testIsCorrectTotalOfPurchases_correctAmount(){
        Assert.assertEquals(24.81, userService.getTotalUserPurchases(10), 0.0001);
    }

    @Test
    public void findUserByEmail_testIsCorrectUser_correctUser(){
        User user = userService.findById(23);
        Assert.assertEquals(user, userService.findByEmail("test2@gmail.com"));
    }

}
