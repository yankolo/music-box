package arquillian;

import com.javaproject.musicbox.entities.Ad;
import com.javaproject.musicbox.filters.AuthorizationFilter;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.AdService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.arquillian.persistence.*;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.ejb.EJBException;
import javax.inject.Inject;
import java.util.List;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class AdServiceTest {

    @Inject
    private AdService adService;

    @Deployment
    public static JavaArchive createDeployment()
    {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(AbstractService.class)
                .addClass(AdService.class)
                .addClass(Ad.class)
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */

    @Test
    public void injectService_testIsCorrectlySetup_serviceInjected() {
        Assert.assertNotNull(adService);
    }

    @Test
    public void create_validAd_AdCreated() {
        Ad expectedAd = generateTestAd();

        adService.create(expectedAd);
        Ad actualAd = adService.findById(3);

        Assert.assertEquals(expectedAd, actualAd);
    }

    @Test
    public void findById_AdExistsInDb_CorrectAdFound() {
        Ad expectedAd = new Ad();
        expectedAd.setId(1);
        expectedAd.setImageURL("/resources/images/osheaga-ads.jpg");
        expectedAd.setWebsiteURL("https://www.osheaga.com/en");
        expectedAd.setActiveAsAd1(true);
        expectedAd.setActiveAsAd2(false);

        Ad actualAd = adService.findById(1);

        Assert.assertEquals(expectedAd, actualAd);
    }

    @Test
    public void update_validAd_AdUpdated() {
        Ad expectedAd = generateTestAd();

        adService.create(expectedAd);
        expectedAd.setImageURL("secondImage");
        adService.update(expectedAd);

        Ad actualAd = adService.findById(3);

        Assert.assertEquals(expectedAd, actualAd);
    }

    @Test
    public void delete_validAd_AdDeleted() {
        Ad ad = generateTestAd();

        adService.create(ad);
        adService.delete(ad);

        Ad adInDatabase = adService.findById(3);

        Assert.assertNull(adInDatabase);
    }

    @Test
    public void findAll_2AdsInDb_2AdsFound() {
        List<Ad> ads = adService.findAll();

        Assert.assertEquals(2, ads.size());
    }


    private Ad generateTestAd() {
        Ad ad = new Ad();
        ad.setImageURL("testImage");
        ad.setWebsiteURL("testWebsite");
        ad.setActiveAsAd1(false);
        ad.setActiveAsAd2(false);

        return ad;
    }
    
    @Test
    public void createAd_testIsCorrectlyCreated_correctlyCreated(){
        adService.createAd("/resources/images/jazz-festival.jpg", "https://google.com");
        List<Ad> ads = adService.findAll();
        Assert.assertEquals(ads.get(ads.size() - 1).getImageURL(), "/resources/images/jazz-festival.jpg");
    }

    @Test
    public void activateAd_testIsCorrectlyActivated_correctlyActivated(){
        List<Ad> ads = adService.findAll();
        Ad ad = ads.get(ads.size() -1);
        adService.activateAd(ad.getId(), 1);
        List<Ad> adsAfter = adService.findAll();
        Ad activeAd = adsAfter.get(adsAfter.size() -1);
        Assert.assertTrue(activeAd.isActiveAsAd1());
    }

    @Test
    public void deactivateAd_testIsCorrectlyDeactivated_correctlyDeactivated(){
        List<Ad> ads = adService.findAll();
        Ad ad1= ads.get(ads.size() -2);
        Ad ad2 = ads.get(ads.size() -1);
        adService.activateAd(ad1.getId(), 1);
        adService.activateAd(ad2.getId(), 1);
        Ad deactivatedAd = adService.findById(ad1.getId());
        Assert.assertFalse(deactivatedAd.isActiveAsAd1());
    }

    @Test
    public void getActiveAd_testIsCorrectAd_correctAd(){
        Ad activeAd = adService.getActiveAd(1);
        Assert.assertEquals(1, activeAd.getId());
    }
    @Test(expected = EJBException.class)
    public void getActiveAdWithInvalidPosition_testIsExceptionThrown_illegalArgument(){
        adService.getActiveAd(3);
    }
}
