package arquillian;

import com.javaproject.musicbox.entities.Survey;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.SurveyService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.ApplyScriptAfter;
import org.jboss.arquillian.persistence.CreateSchema;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class SurveyServiceTest {

    @Inject
    private SurveyService surveyService;

    @Deployment
    public static JavaArchive createDeployment(){
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void createSurvey_testIsCorrectlyCreate_correctlyCreated(){
        List<String> options = new ArrayList<>();
        options.add("Pop");
        options.add("Rock");
        options.add("Jazz");
        options.add("Classical");
        surveyService.createSurvey("Test", options);
        List<Survey> surveys = surveyService.findAll();
        Survey latestSurvey = surveys.get(surveys.size() -1);
        Assert.assertEquals(latestSurvey.getQuestion(), "Test");
    }

    @Test
    public void setActiveSurvey_testIsActiveSurveyCorrectlySet_correctlySet(){
        surveyService.setActiveSurvey(2);
        Assert.assertTrue(surveyService.findById(2).isActive());
    }

    @Test
    public void getSurveyOptions_testIsCorrectSetOfOptions_correctSetOfOptions(){
        List<String> expectedOptions = new ArrayList<>();
        expectedOptions.add("Pop");
        expectedOptions.add("Rock");
        expectedOptions.add("Jazz");
        expectedOptions.add("Classical");
        Assert.assertEquals(surveyService.getSurveyOptions(), expectedOptions);
    }

    @Test
    public void getSurveyQuestion_testIsCorrectSurveyQuestion_correctActiveQuestion(){
        Assert.assertEquals(surveyService.getSurveyQuestion(), "What is your favorite genre?");
    }

    @Test
    public void getNumberOfVotes_testIsCorrectNumberOfVotesForEachOption_correctNumberOfVotes(){
        List<Long> optionVotes = new ArrayList<>();
        for(int i=0; i<4; i++){
            optionVotes.add((long) 0);
        }
        Assert.assertEquals(optionVotes, surveyService.getNumberOfVotes());
    }

    @Test
    public void getTotalSurveyVotes_testIsCorrectTotalOfVotes_correctTotal(){
        Assert.assertEquals(0, surveyService.getTotalVotes());
    }

}
