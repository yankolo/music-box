package arquillian;

import com.javaproject.musicbox.entities.Answer;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.OrderItem;
import com.javaproject.musicbox.entities.OrderItemPK;
import com.javaproject.musicbox.entities.Review;
import com.javaproject.musicbox.entities.ShoppingCart;
import com.javaproject.musicbox.entities.Survey;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.AbstractService;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.ReviewService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.*;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

@RunWith(Arquillian.class)
@CreateSchema("create_tables.sql")
@ApplyScriptAfter("create_tables.sql")
public class ReviewServiceTest {

    @Inject
    private ReviewService reviewService;

    @Inject
    private ItemService itemService;
    
    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true,"com.javaproject.musicbox")
                .addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    /**
     * Following test naming convention whatIsTested_theCondition_ExpectedResult
     */
    @Test
    public void injectService_testIsCorrectlySetup_reviewServiceInjected() {
        Assert.assertNotNull(reviewService);
    }

    @Test
    public void getApprovedReviewsForGivenItem_validIdOfItem_resultListOfApprovedReviews() {
        Assert.assertEquals(2, reviewService.getAllReviewsForGivenItem(1).size());
    }

    @Test
    public void getAllApprovedReviews_isApprovedTrue_tenApproved() {
        Assert.assertEquals(10, reviewService.getAllReviewsByCriteria(true).size());
    }

    /**
     * Testing the AbstractService class which is implemented by every service
     */
    @Test
    public void findReview_testIsCorrectlySetup_findReviewWithId1() {
        Review r = reviewService.findById(1);
        Assert.assertEquals(1, r.getReviewId());
    }
    
    public void getApprovedreviews_isApprovedTrue_twoApproved() {
                Assert.assertEquals(2, reviewService.getAllReviewsForGivenItem(1).size());
    
    }
}
