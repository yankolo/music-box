----------------------------------
-- NEEDS TO BE RUN BY ROOT USER --
----------------------------------

DROP DATABASE IF EXISTS CSteam_c;
CREATE DATABASE CSteam_c;

USE CSteam_c;

DROP USER IF EXISTS CSteam_c@localhost;
CREATE USER CSteam_c@'localhost' IDENTIFIED WITH mysql_native_password BY 'matickem' REQUIRE NONE;
GRANT ALL ON CSteam_c.* TO CSteam_c@'localhost';

FLUSH PRIVILEGES;
