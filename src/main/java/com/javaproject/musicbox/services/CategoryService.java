/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Category;

import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 * @author Danny
 */
@Stateless
public class CategoryService extends AbstractService<Category> implements Serializable {
    public CategoryService() {
        super(Category.class);
    }

    /**
     * 
     * @param name
     * @return 
     */
    public Category findByName(String name) {
        return em.createQuery("SELECT c FROM Category c WHERE c.name = :category", Category.class)
                .setParameter("category", name)
                .getSingleResult();
    }
    
    public void createCategory(String name){
        Category c = new Category();
        c.setName(name);
        em.persist(c);
    }
}
