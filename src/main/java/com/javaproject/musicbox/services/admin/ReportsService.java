/*
 * Service to show reports about tracks, albums and artists
 */
package com.javaproject.musicbox.services.admin;

import com.javaproject.musicbox.entities.Artist;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.User;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author Lara
 */
@Stateless
public class ReportsService implements Serializable {

    @PersistenceContext(unitName = "musicpu")
    protected EntityManager em;

    /**
     * Method that finds all the items that were ordered between a given date
     *
     * @param start
     * @param end
     * @return
     */
    public List<Order> getAllOrderedItems(Date start, Date end) {
        return em.createQuery("SELECT o from Order o")//o WHERE o.timestamp BETWEEN :start AND :end")
                //            .setParameter("start", start, TemporalType.DATE)
                //            .setParameter("end", end, TemporalType.DATE)              
                .getResultList();
    }

    public double getTotalCost(Date start, Date end) {
        return (Double) em.createQuery("SELECT SUM(i.listPrice) from Item i JOIN i.orderItems io")// JOIN Order o where o.timestamp BETWEEN :start AND:end")
                //            .setParameter("start", start, TemporalType.DATE)
                //            .setParameter("end", end, TemporalType.DATE)

                .getResultList().get(0);
    }

    /**
     * Select all users from the database
     *
     * @return
     */
    public List<User> getAllUsers() {
        return em.createQuery("SELECT u from User u").getResultList();
    }

    public List<Artist> getAllArtists() {
        return em.createQuery("SELECT a from Artist a").getResultList();
    }

    public List<Item> getAllUsersItem(User user) {
        return em.createQuery("SELECT i FROM Item i JOIN i.orderItems io WHERE io.order.user = :user")
                .setParameter("user", user)
                .getResultList();
    }

    public double getTotalUserPayed(User user) {
        return (Double) em.createQuery("SELECT SUM(o.grandTotal) FROM Order o WHERE o.user = :user")
                .setParameter("user", user)
                .getSingleResult();
    }

    public List<Item> getAllItemsWithType(String type) {
        return em.createQuery("SELECT i FROM Item i WHERE i.itemType = :type")
                .setParameter("type", type)
                .getResultList();
    }

    public List<Order> getOrdersForItem(int itemId) {
        return em.createQuery("SELECT o from Order o JOIN o.orderItems oi WHERE oi.item.itemId = :itemId")//o WHERE o.timestamp BETWEEN :start AND :end")
                //            .setParameter("start", start, TemporalType.DATE)
                //            .setParameter("end", end, TemporalType.DATE)              
                .getResultList();
    }

    public List<Item> getAllItems() {
        return em.createQuery("SELECT i FROM Item i").getResultList();
    }
 }
