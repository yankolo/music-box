package com.javaproject.musicbox.services;


import com.javaproject.musicbox.annotations.CurrentCart;
import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.ShoppingCart;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.eventsdata.LoginEventData;
import com.javaproject.musicbox.eventsdata.RegistrationEventData;

import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

import static com.javaproject.musicbox.util.SessionUtilities.getSessionCart;

/**
 * The ShoppingCartService is a data access class that is responsible for the
 * ShoppingCart Entity
 */
@Stateless
public class ShoppingCartService extends AbstractService<ShoppingCart> implements Serializable {
    @LoggedInUser
    @Inject
    private Instance<User> user; // To get the user's shopping cart

    public ShoppingCartService() {
        super(ShoppingCart.class);
    }

    /**
     * Whenever a user registers in the website, an empty cart is made for him
     *
     * @param registrationEventData Data sent from the registration event
     */
    public void observeRegistrationEvent(@Observes RegistrationEventData registrationEventData) {
        User registeredUser = registrationEventData.getRegisteredUser();

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setUser(registeredUser);

        registeredUser.setShoppingCart(shoppingCart);

        create(shoppingCart);
    }

    /**
     * Whenever a user logs in the website, the items he/she had in the cart
     * will be transferred to his own personal persisted shopping cart
     *
     * @param loginEventData Data send from the login event
     */
    public void observeLoginEvent(@Observes LoginEventData loginEventData) {
        User loggedInUser = loginEventData.getLoggedInUser();

        ShoppingCart userCart = loggedInUser.getShoppingCart();
        ShoppingCart sessionCart = getSessionCart();

        List<Item> itemsInSessionCart = sessionCart.getItems();
        List<Item> itemsInUserCart = userCart.getItems();

        itemsInUserCart.addAll(itemsInSessionCart);
        update(userCart);

        itemsInSessionCart.clear();
    }

    /**
     * The following method gets the current cart. It either gets it from
     * the session or from the database (depending if the user is currently
     * logged in or not)
     *
     * @return The current shopping cart
     */
    @Produces
    @CurrentCart
    public ShoppingCart getCurrentCart() {
        ShoppingCart currentCart = null;

        if (user.get() == null) {
            currentCart = getSessionCart();
        } else {
            currentCart = user.get().getShoppingCart();
        }

        return currentCart;
    }

    /**
     * Adds an item to the current shopping cart (i.e. the current user's
     * shopping cart, which is either his session cart or his persisted
     * cart)
     *
     * @param item
     */
    public void addItemToCurrentCart(Item item) {
        ShoppingCart cart = getCurrentCart();
        cart.addItem(item);

        // Persist change to DB if the cart is not a session cart
        if (!cart.equals(getSessionCart())) {
            update(cart);
        }
    }

    /**
     * Remove an item from the current shopping cart (i.e. the current user's
     * shopping cart, which is either his session cart or his persisted
     * cart)
     *
     * @param item
     */
    public void removeItemFromCurrentCart(Item item) {
        ShoppingCart cart = getCurrentCart();
        cart.removeItem(item);

        // Persist change to DB if the cart is not a session cart
        if (!cart.equals(getSessionCart())) {
            update(cart);
        }
    }
}
