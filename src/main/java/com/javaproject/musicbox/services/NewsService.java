/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Ad;
import com.javaproject.musicbox.entities.News;
import java.io.Serializable;
import javax.ejb.Stateless;

/**
 *
 * @author Danny
 */
@Stateless
public class NewsService extends AbstractService<News> implements Serializable {
    
    public NewsService(){
        super(News.class);
    }
    
    /**
     * Create a news
     * @param webURL
     */
    public void createNews(String webURL){
        News news = new News();
        news.setUrl(webURL);
        em.persist(news);
    }
    
    /**
     * Set makes the specified news activate and the old news deactivated 
     * @param newsId 
     */
    public void setActivatedNews(int newsId){
        News oldNews = getActivatedNews();
        oldNews.setActiveNews(false);
        
        News newNews = super.findById(newsId);
        newNews.setActiveNews(true);
        
        em.persist(oldNews);
        em.persist(newNews);
    }
    
    /**
     * Get activated News
     * @return News 
     */
    public News getActivatedNews(){
        return em.createQuery("SELECT n FROM News n WHERE n.activeNews = true", News.class).getSingleResult();
    }
    
}
