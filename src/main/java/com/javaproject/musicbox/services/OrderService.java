package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.OrderItem;
import com.javaproject.musicbox.entities.OrderItemPK;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.io.Serializable;

@Stateless
public class OrderService extends AbstractService<Order> implements Serializable {
    public OrderService() {
        super(Order.class);
    }

    /**
     * Finds an orderItem by its composite primary key
     *
     * @return The OrderItem
     */
    public OrderItem findOrderItemByPK(int orderId, int itemId) {
        TypedQuery<OrderItem> query = em.createQuery("SELECT oi from OrderItem oi " +
                "WHERE oi.orderItemPK.orderId = :orderId  " +
                "AND oi.orderItemPK.itemId = :itemId", OrderItem.class);
        query.setParameter("orderId", orderId);
        query.setParameter("itemId", itemId);

        return query.getResultList().get(0);
    }
}