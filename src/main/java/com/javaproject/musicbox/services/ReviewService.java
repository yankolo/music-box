/*
 * The ReviewService is a data access class that is responsible for the
 * Review Entity
 */
package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Review;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;

/**
 * @author Lara
 */
@Stateless
public class ReviewService extends AbstractService<Review> implements Serializable {

    public ReviewService() {
        super(Review.class);
    }

    /**
     * A method that finds an item by a given id and returns all of the reviews
     * that have been approved for that item
     *
     * @param id
     * @return
     */
    public List<Review> getAllReviewsForGivenItem(int id) {

        return em.createQuery("SELECT r FROM Review r WHERE r.itemId = :id AND r.isApproved = true", Review.class)
                .setParameter("id", em.find(Item.class, id)).getResultList();
        //ask About review list!
    }

    /**
     * A method that gets all reviews that are approved/refused according to the given param 
     *
     * @param isApproved
     * @return
     */
    public List<Review> getAllReviewsByCriteria(boolean isApproved) {
        return em.createQuery("SELECT r FROM Review r WHERE r.isApproved = :isApproved", Review.class)
                .setParameter("isApproved", isApproved).getResultList();
    }
}//end class
