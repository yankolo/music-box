package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Ad;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import java.io.Serializable;

/**
 * Service class for Ads - used by Ad management to create, read and set active ads.
 */
@Stateless
public class AdService extends AbstractService<Ad> implements Serializable {

    public AdService(){
        super(Ad.class);
    }

    /**
     * Create a new Ad
     * @param imgURL
     * @param webURL
     */
    public void createAd(String imgURL, String webURL){
        Ad ad = new Ad();
        ad.setImageURL(imgURL);
        ad.setWebsiteURL(webURL);
        em.persist(ad);
    }

    /**
     * Activate the Ad with a given ID and de-activate the previous one.
     * @param adId
     */
    public void activateAd(int adId, int position){
        Ad adToActivate = findById(adId);
        //Get the current active ad and deactivate it
        Ad previousActiveAd = getActiveAd(position);
        if(position == 1){
            previousActiveAd.setActiveAsAd1(false);
            adToActivate.setActiveAsAd1(true);
        }
        else{
            previousActiveAd.setActiveAsAd2(false);
            adToActivate.setActiveAsAd2(true);
        }
        //Persist changes
        update(previousActiveAd);
        update(adToActivate);
    }

    /**
     * Get an Ad instance for the specified active position.
     * @param adPosition
     * @return the active ad #1 or #2
     */
    public Ad getActiveAd(int adPosition){
        String queryParam;
        if(adPosition == 1){
            queryParam = "a.isActiveAsAd1 = true ";
        }
        else if(adPosition == 2){
            queryParam = "a.isActiveAsAd2 = true ";
        }
        else{
            throw new IllegalArgumentException("Invalid ad position - choose either 1 or 2");
        }
        Ad activeAd;
        try {
            activeAd = (Ad) em.createQuery("SELECT a FROM Ad a WHERE " + queryParam).getSingleResult();
        }
        catch(NoResultException nre){
            return null;
        }
        return activeAd;
    }
}
