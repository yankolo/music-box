package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Answer;
import com.javaproject.musicbox.entities.Survey;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.util.SessionUtilities;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Service class for Surveys - supports multiple surveys with one being active.
 */
@Stateless
public class SurveyService extends AbstractService<Survey> implements Serializable {
    @Inject
    private UserService userDAO;

    public SurveyService(){
        super(Survey.class);
    }

    /**
     * Method used by the manager side to create a new survey.
     * @param question
     * @param options
     */
    public void createSurvey(String question, List<String> options){
        Survey survey = new Survey();
        survey.setQuestion(question);
        survey.setOption1(options.get(0));
        survey.setOption2(options.get(1));
        survey.setOption3(options.get(2));
        survey.setOption4(options.get(3));
        em.persist(survey);
    }

    /**
     * Method used by the manager side to set the active survey.
     * @param surveyID
     */
    public void setActiveSurvey(int surveyID){
        //Get the current active survey and set to inactive
        Survey activeSurvey = getActiveSurvey();
        activeSurvey.setActive(false);
        em.persist(activeSurvey);

        //Get the survey to be set to active and activate it
        Survey surveyToActivate = getSurvey(surveyID);
        surveyToActivate.setActive(true);
        em.persist(surveyToActivate);
    }

    /**
     * Helper method to get the currently active survey instance.
     * @return active Survey
     */
    private Survey getActiveSurvey(){
        return (Survey) em.createQuery("SELECT s FROM Survey s WHERE s.isActive = true").getSingleResult();
    }

    /**
     * Helper method to fetch a survey with a specific ID
     * @param surveyID
     * @return survey with ID specified
     */
    private Survey getSurvey(int surveyID){
        return (Survey) em.createQuery("SELECT s FROM Survey s WHERE s.surveyId = :surveyID")
                .setParameter("surveyID", surveyID).getSingleResult();
    }

    /**
     * Get a List with all the options of the survey
     *
     * @return survey options list
     */
    public List<String> getSurveyOptions(){
        Query getSurvey = em.createQuery("SELECT s FROM Survey s WHERE s.isActive = true");
        Survey survey = (Survey) getSurvey.getSingleResult();
        List<String> options = new ArrayList<>();
        options.add(survey.getOption1());
        options.add(survey.getOption2());
        options.add(survey.getOption3());
        options.add(survey.getOption4());
        return options;
    }

    /**
     * Get the question of the survey
     *
     * @return survey question
     */
    public String getSurveyQuestion(){
        Query getSurvey = em.createQuery("SELECT s FROM Survey s WHERE s.isActive = true");
        Survey survey = (Survey) getSurvey.getSingleResult();
        return survey.getQuestion();
    }

    /**
     * Saves the user's survey answer to the database. If a user has already answered the poll previously, the entry is
     * updated accordingly.
     * @param answer
     */
    public void submitSurveyAnswer(int answer){
        //Get the currently signed in User instance
        User currentUser = SessionUtilities.getSessionUser();
        Answer userAnswer = getUserAnswer();
        if(userAnswer == null){
            userAnswer = new Answer();
        }
        userAnswer.setAnswer(answer);
        userAnswer.setUser(currentUser);
        userAnswer.setSurveyId((Survey) em.createQuery("SELECT s FROM Survey s WHERE s.isActive = true").getSingleResult());
        currentUser.getAnswers().add(userAnswer);
        em.persist(userAnswer);
        userDAO.update(currentUser);
    }

    /**
     * Get the existing survey answer of the currently signed in user.
     * @return the answer of the user if they have already answered
     */
    public Answer getUserAnswer(){
        User currentUser = userDAO.getCurrentUser();
        Answer userAnswer;
        try {
            //Get the user's answer for the active survey
             userAnswer = (Answer) em.createQuery("SELECT a FROM Answer a INNER JOIN a.surveyId s WHERE a.user.userId = :currentUser AND s.isActive = true")
                    .setParameter("currentUser", currentUser.getUserId()).getSingleResult();
        }
        catch(NoResultException e){
            return null;
        }
        return userAnswer;
    }

    /**
     * Get the number of votes for each option of a survey.
     *
     * @return a List of amount of votes for each option
     */
    public List<Long> getNumberOfVotes() {
        List<Long> numVotes = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            numVotes.add(getNumberOfVotesForOption(i));
        }
        return numVotes;
    }

    /**
     * Get the number of votes for each option in the active survey.
     * @param option
     * @return the number of votes in the active survey for a specific option
     */
    private long getNumberOfVotesForOption(int option){
        return (long) em.createQuery("SELECT count(a.answer) FROM Answer a INNER JOIN a.surveyId s WHERE a.answer=:option AND s.isActive = true")
                .setParameter("option", option).getSingleResult();
    }

    /**
     * Get the total number of votes of the active survey.
     * @return the total number of votes in the survey
     */
    public long getTotalVotes(){
        return (long) em.createQuery("SELECT count(a.answer) FROM Answer a INNER JOIN a.surveyId s WHERE s.isActive = true").getSingleResult();
    }
}
