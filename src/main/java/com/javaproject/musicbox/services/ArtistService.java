/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Artist;
import javax.ejb.Stateless;

/**
 *
 * @author Danny
 */
@Stateless
public class ArtistService extends AbstractService<Artist>{
    
    public ArtistService(){
        super(Artist.class);
    }
    
    public void createArtist(String name){
        Artist a = new Artist();
        a.setName(name);
        em.persist(a);
    }
}
