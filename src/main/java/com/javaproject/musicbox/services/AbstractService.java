package com.javaproject.musicbox.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * The AbstractService contains the common functionality for almost all the services,
 * for example, create(), findById(), update(), delete() ...
 *
 * @param <T> The type of the Entity
 */

public abstract class AbstractService<T> {
    @PersistenceContext(unitName = "musicpu")
    protected EntityManager em;

    private Class<T> entityClass;

    public AbstractService() {
    }

    public AbstractService(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Creates an entity (i.e. takes an entity object and persists it)
     *
     * @param entity The entity to create
     * @return The same entity that was created
     */
    public T create(T entity) {
        em.persist(entity);
        return entity;
    }

    /**
     * Finds an entity by id
     *
     * @param id The id of the entity to find
     * @return The entity that has the id that was searched
     */
    public T findById(int id) {
        return em.find(entityClass, id);
    }

    /**
     * Updates an entity
     *
     * @param entity The entity to update
     * @return The same entity that was updated
     */
    public T update(T entity) {
        return em.merge(entity);
    }

    /**
     * Deletes an entity from the database
     *
     * @param entity The entity to remove
     */
    public void delete(T entity) {
        em.remove(em.merge(entity));
    }

    /**
     * Find all the entities
     *
     * @return
     */
    public List<T> findAll() {
        CriteriaQuery<T> criteria = em.getCriteriaBuilder().createQuery(entityClass);
        TypedQuery<T> typedQuery = em.createQuery(criteria.select(criteria.from(entityClass)));
        return typedQuery.getResultList();
    }

}
