package com.javaproject.musicbox.services;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.eventsdata.LoginEventData;
import com.javaproject.musicbox.eventsdata.RegistrationEventData;
import com.javaproject.musicbox.util.EncryptionUtilities;
import com.javaproject.musicbox.util.SessionUtilities;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * The UserService is a data access class that is responsible for the User
 * Entity
 */
@Stateless
public class UserService extends AbstractService<User> implements Serializable {
    @Inject
    private Event<LoginEventData> loginEvent;
    @Inject
    private Event<RegistrationEventData> registrationEvent;
    @Inject
    private CategoryService categoryService;

    public UserService() {
        super(User.class);
    }

    public User findByEmail(String email) {
        TypedQuery<User> query = em.createQuery("SELECT user FROM User user WHERE user.email = :email", User.class);
        query.setParameter("email", email);

        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    /**
     * Register the user by populating the hash and salt fields of the user
     * and persisting the user
     *
     * @param user The user to populate
     */
    public void registerUser(User user) throws InvalidKeySpecException, NoSuchAlgorithmException {
        // Generate salt & hash from password
        String salt = EncryptionUtilities.generateSalt();
        byte[] hash = EncryptionUtilities.generateHash(user.getPassword(), salt);

        // Store the generated hash and salt in the user
        user.setHash(hash);
        user.setSalt(salt);

        create(user);

        // Fire the registration event
        RegistrationEventData registrationEventData = new RegistrationEventData(user);
        registrationEvent.fire(registrationEventData);
    }

    /**
     * Logs in an user into their account
     *
     * @param email
     * @param password
     * @return true if the login was successful
     */
    public boolean loginUser(String email, String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        // Getting the user that is trying to log in
        User user = findByEmail(email);

        if (user == null)
            return false;

        // If password matches login the user
        if (EncryptionUtilities.passwordMatches(password, user.getHash(), user.getSalt())) {
            // Store user in the session
            SessionUtilities.setSessionUser(user);

            // Fire login event
            LoginEventData loginEventData = new LoginEventData(user);
            loginEvent.fire(loginEventData);

            return true;
        } else {
            return false;
        }
    }

    public void changePassword(User user, String newPassword) throws InvalidKeySpecException, NoSuchAlgorithmException {
        // Generate salt & hash from password
        String salt = EncryptionUtilities.generateSalt();
        byte[] hash = EncryptionUtilities.generateHash(newPassword, salt);

        // Store the generated hash and salt in the user
        user.setHash(hash);
        user.setSalt(salt);

        update(user);
    }

    /**
     * Get the user that is currently signed in OR null if no user is logged in
     *
     * @return user object
     */
    @Produces
    @LoggedInUser
    public User getCurrentUser() {
        User currentUser = SessionUtilities.getSessionUser();
        return currentUser;
    }

    /**
     * Public helper method used by the registration mechanism to check if an email is already in use
     *
     * @param email
     * @return true if the email is already in use
     */
    public boolean checkEmailUsed(String email) {
        User user = findByEmail(email);
        return user != null;
    }

    /**
     * Sets last visited category of specific user
     *
     * @param user
     * @param categoryName
     */
    public void setLastVisitedCategory(User user, String categoryName) {
        Category category = categoryService.findByName(categoryName);
        
        user.setLastVisitedCategory(category);
        update(user);
    }

    /**
     * Calculates the total value of all purchases done by a client.
     * @param userID the client whose total purchases we want to get
     * @return total amount in $ of client purchases
     */
    public double getTotalUserPurchases(int userID){
        //Get the user with specified ID and their list of orders/purchases
        User user = findById(userID);
        List<Order> userOrders = user.getOrders();
        //If the user hasn't purchased anything yet
        if(userOrders.isEmpty()){
            return 0;
        }
        //Sum up the total of all orders
        double totalOfPurchases = 0;
        for(Order order : userOrders){
            totalOfPurchases += order.getGrandTotal();
        }
        return totalOfPurchases;
    }
}
