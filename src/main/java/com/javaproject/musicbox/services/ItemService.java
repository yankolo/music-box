package com.javaproject.musicbox.services;

import com.javaproject.musicbox.entities.Album;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Review;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.enums.FilterType;
import com.javaproject.musicbox.enums.SearchFilter;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * The ItemService is a data access class that is responsible for the Item
 * Entity
 *
 * @Author Lara Danny
 */
@Stateless
public class ItemService extends AbstractService<Item> implements Serializable {
    public ItemService() {
        super(Item.class);
    }

    /**
     * Returns all albums in the database
     * 
     * @return 
     */
    public List<Album> getAllAlbums(){
        TypedQuery<Album> findAlbums
                = em.createQuery("SELECT a FROM Album a ", Album.class);
        return findAlbums.getResultList();
    }
    
    

    /**
     * Finds most recently added items in the database
     *
     * @param threshold
     * @return
     */
    public List<Item> findLatest(int threshold) {
        return em.createQuery("SELECT i FROM Item i WHERE i.dateRemoved IS NULL ORDER BY i.dateAdded DESC", Item.class)
                .setMaxResults(threshold).getResultList();
    }

    /**
     * Finds recommended items for specific user
     * Algorithm: gets the latest in the last visited category of the user
     *
     * @param threshold
     * @param user
     * @return
     */
    public List<Item> findRecommend(int threshold, User user) {
        String categoryName = user.getLastVisitedCategory().getName();

        return em.createQuery("SELECT i FROM Item i JOIN i.categoryList c WHERE c.name = :category AND i.dateRemoved IS NULL ORDER BY i.dateAdded DESC", Item.class)
                .setParameter("category", categoryName)
                .setMaxResults(threshold).getResultList();
    }
    
    /**
     * Finds items on sale
     * 
     * @param threshold
     * @return 
     */
    public List<Item> findSpecials(int threshold){
        return em.createQuery("SELECT i FROM Item i WHERE i.salesPrice IS NOT NULL AND i.dateRemoved IS NULL ORDER BY i.dateAdded DESC", Item.class)
                .setMaxResults(threshold).getResultList();
    }

    /**
     * Gets user's last visited category
     *
     * @param user
     * @return
     */
    public Category getLastVisitedCategory(String user) {
        return em.createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", user).getSingleResult().getLastVisitedCategory();
    }
    
    /**
     * Multiple queries that could match to the string criteria
     *
     * @param criteria
     * @param filterTypeList
     * @param searchFilter
     * @param from
     * @param to
     * @return List<Item>
     */
    public List<Item> findByCriteria(String criteria, List<FilterType> filterTypeList, List<SearchFilter> searchFilter, Date from, Date to) {
        List<Item> list = new ArrayList<>();

        list = getListBySearchFilter(criteria, searchFilter);
        list = getTypeFiltered(filterTypeList, list);
        list = getByDateRange(searchFilter, from, to, list);
        return list;
    }

    /**
     * Check the searchFilter has DATES in the list to filter by dates
     * 
     * @param searchFilter
     * @param from
     * @param to
     * @param list
     * @return 
     */
    private List<Item> getByDateRange(List<SearchFilter> searchFilter, Date from, Date to, List<Item> list) {
        for (SearchFilter sf : searchFilter) {
            switch (sf) {
                case DATES:
                    return filterByDateRange(from, to, list);
            }
        }

        return list;
    }

    /**
     * Adds to the list according to the specified SearchFilter except DATES
     * 
     * @param criteria
     * @param searchFilter
     * @return 
     */
    private  List<Item> getListBySearchFilter(String criteria, List<SearchFilter> searchFilter) {
        List<Item> list = new ArrayList<>();

        for (SearchFilter sf : searchFilter) {
            switch (sf) {
                case LABEL:
                    addNonDuplicateList(list, findByLabel(criteria));
                    break;
                case ARTIST:
                    addNonDuplicateList(list, findByArtist(criteria));
                    break;
                case TITLE:
                    addNonDuplicateList(list, findByTitle(criteria));
                    break;
                case GENRE:
                    addNonDuplicateList(list, findByGenre(criteria));
                    break;
            }
        }

        return list;
    }
    
    /**
     * Filters list according to specified types like track and album
     * 
     * @param filterTypeList
     * @param list
     * @return 
     */
    private List<Item> getTypeFiltered(List<FilterType> filterTypeList, List<Item> list) {
        List<Item> returnList = new ArrayList<>();

        for (Item i : list) {
            for (FilterType ft : filterTypeList) {
                if (i.getItemType().toLowerCase().equals(ft.toString().toLowerCase())) {
                    returnList.add(i);
                }
            }
        }

        return returnList;
    }
    
    /**
     * Filters the list according to a date range
     * 
     * @param from
     * @param to
     * @param list
     * @return 
     */
    private List<Item> filterByDateRange(Date from, Date to, List<Item> list) {

        List<Item> returnList = new ArrayList<>();
        
        for (Item i : list) {
            if (i.getDateAdded().after(from) && i.getDateAdded().before(to)) {
                returnList.add(i);
            }
        }
        
        return returnList;

    }

    /**
     * 
     * @param title
     * @return 
     */
    private List<Item> findByTitle(String title) {
        // Generic Item search
        TypedQuery<Item> findByName
                = em.createQuery("SELECT i FROM Item i WHERE i.name LIKE :name AND i.dateRemoved IS NULL", Item.class)
                        .setParameter("name", "%" + title + "%");
        return findByName.getResultList();
    }

    /**
     * 
     * @param genre
     * @return 
     */
    private List<Item> findByGenre(String genre) {
        TypedQuery<Item> findByCategory
                = em.createQuery("SELECT i FROM Item i JOIN i.categoryList c WHERE c.name LIKE :genre AND i.dateRemoved IS NULL", Item.class)
                        .setParameter("genre", "%" + genre + "%");
        return findByCategory.getResultList();
    }

    /**
     * 
     * @param label
     * @return 
     */
    private List<Item> findByLabel(String label) {

        // Specific Item type ("Album") search
        TypedQuery<Item> findByAlbumLabel
                = em.createQuery("SELECT a FROM Album a WHERE a.label LIKE :label AND a.dateRemoved IS NULL", Item.class)
                        .setParameter("label", "%" + label + "%");
        return findByAlbumLabel.getResultList();
    }

    /**
     * 
     * @param artist
     * @return 
     */
    private List<Item> findByArtist(String artist) {
        TypedQuery<Item> findByAlbumArtist
                = em.createQuery("SELECT a FROM Album a JOIN a.artist ar WHERE ar.name LIKE :name AND a.dateRemoved IS NULL", Item.class)
                        .setParameter("name", "%" + artist + "%");

        // Specific Item type ("Track") search
        TypedQuery<Item> findByTrackArtist
                = em.createQuery("SELECT t FROM Track t JOIN t.artistList a WHERE a.name LIKE :name AND t.dateRemoved IS NULL", Item.class)
                        .setParameter("name", "%" + artist + "%");

        List<Item> list = new ArrayList<>();

        addNonDuplicateList(list, findByAlbumArtist.getResultList());
        addNonDuplicateList(list, findByTrackArtist.getResultList());

        return list;

    }

    /**
     * Helped method to merge 2 lists together without duplicate
     *
     * @param list
     * @param toMerge
     */
    private void addNonDuplicateList(List<Item> list, List<Item> toMerge) {
        for (Item i : toMerge) {
            if (!list.contains(i)) {
                list.add(i);
            }
        }
    }

    /**
     * This method returns a list of 3 items (Albums) that share the same genre
     * of the currently viewed item without including it
     *
     * @param genre
     * @param itemId
     * @param type
     * @return
     */
    public List<Item> getAllItemsWithSameGenre(String genre, int itemId, String type) {
        return em.createQuery("SELECT i from Item i JOIN i.categoryList c WHERE c.name = :genre AND NOT i.itemId = :itemId AND i.itemType = :type AND i.dateRemoved IS NULL", Item.class)
                .setParameter("genre", genre)
                .setParameter("itemId", itemId)
                .setParameter("type", type)
                .setMaxResults(3).getResultList();
    }
}
