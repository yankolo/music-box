/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Yanik
 */
@Entity
@Table(name = "order_item")
public class OrderItem implements Serializable {
    @EmbeddedId
    private OrderItemPK orderItemPK;

    @Column(name = "price")
    private double price;

    @ManyToOne
    @MapsId(value = "itemId")
    @JoinColumn(name="item_id", referencedColumnName="item_id", nullable=false)
    private Item item;

    @ManyToOne
    @MapsId(value = "orderId")
    @JoinColumn(name="order_id", referencedColumnName="order_id", nullable=false)
    private Order order;

    public OrderItem() {
    }

    public OrderItem(OrderItemPK orderItemPK) {
        this.orderItemPK = orderItemPK;
    }

    public OrderItem(OrderItemPK orderItemPK, double price) {
        this.orderItemPK = orderItemPK;
        this.price = price;
    }

    public OrderItemPK getOrderItemPK() {
        return orderItemPK;
    }

    public void setOrderItemPK(OrderItemPK orderItemPK) {
        this.orderItemPK = orderItemPK;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(orderItemPK, orderItem.orderItemPK);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderItemPK);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "orderItemPK=" + orderItemPK +
                ", price=" + price +
                '}';
    }
}
