/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Queen
 */
@Entity
@DiscriminatorValue("track")
public class Track extends Item implements Serializable {
    @Column(name = "length")
    private String length;

    @Column(name = "track_num")
    private String trackNum;

    @Column(name = "songwriters")
    private String songwriters;

    @ManyToMany(mappedBy = "trackCollection")
    private List<Artist> artistList;

    @JoinColumn(name = "album_id", referencedColumnName = "item_id")
    @ManyToOne
    private Album album;

    public Track() {
        artistList = new ArrayList<>();
    }

    public Track(int itemId) {
        super(itemId, "track");
    }

    public Track(int itemId, String length, String songwriters) {
        super(itemId, "track");
        this.length = length;
        this.songwriters = songwriters;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getTrackNum() {
        return trackNum;
    }

    public void setTrackNum(String trackNum) {
        this.trackNum = trackNum;
    }

    public String getSongwriters() {
        return songwriters;
    }

    public void setSongwriters(String songwriters) {
        this.songwriters = songwriters;
    }

    public List<Artist> getArtistList() {
        return artistList;
    }

    public void setArtistList(List<Artist> artistList) {
        this.artistList = artistList;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbumId(Album albumId) {
        this.album = albumId;
    }

    @Override
    public String toString() {
        return "Track{" +
                "itemId=" + getItemId() +
                ", length='" + length + '\'' +
                ", trackNum='" + trackNum + '\'' +
                ", songwriters='" + songwriters + '\'' +
//                ", artistList=" + artistList +
                ", album=" + album +
                '}';
    }
}
