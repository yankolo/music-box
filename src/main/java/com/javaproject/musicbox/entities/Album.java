/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Queen
 */
@Entity
@DiscriminatorValue("album")
public class Album extends Item implements Serializable {
    @Column(name = "release_date")
    private Date releaseDate;

    @Column(name = "label")
    private String label;

    @Column(name = "num_tracks")
    private int numTracks;

    @ManyToOne
    @JoinColumn(name = "artist_id", referencedColumnName = "artist_id")
    private Artist artist;

    @OneToMany(mappedBy = "album")
    private List<Track> trackList;

    public Album() {
        trackList = new ArrayList<>();
    }

    public Album(int itemId) {
        super(itemId, "album");
    }

    public Album(int itemId, Date releaseDate, String label, int numTracks) {
        super(itemId, "album");
        this.releaseDate = releaseDate;
        this.label = label;
        this.numTracks = numTracks;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getNumTracks() {
        return numTracks;
    }

    public void setNumTracks(int numTracks) {
        this.numTracks = numTracks;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
    }

    @Override
    public String toString() {
        return "Album{" +
                "itemId=" + getItemId() +
                ", releaseDate=" + releaseDate +
                ", label='" + label + '\'' +
                ", numTracks=" + numTracks +
                ", artist=" + artist +
//                ", trackList=" + trackList +
                '}';
    }
}
