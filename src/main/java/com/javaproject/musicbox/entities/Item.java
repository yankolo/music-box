/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Danny
 */
@Entity
@Table(name = "item")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "item_type")
public abstract class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private int itemId;

    @Column(name = "item_type")
    private String itemType;

    @Column(name = "list_price")
    private double listPrice;

    @Column(name = "cost_price")
    private double costPrice;

    @Column(name = "sales_price")
    private double salesPrice;

    @Column(name = "name")
    private String name;

    @Column(name = "picture_path")
    private String picturePath;

    @Column(name = "date_added")
    private Date dateAdded;

    @Column(name = "date_removed")
    private Date dateRemoved;

    @JoinTable(name = "category_item", joinColumns = {
        @JoinColumn(name = "item_id", referencedColumnName = "item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "category_id", referencedColumnName = "category_id")})
    @ManyToMany
    private List<Category> categoryList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId", fetch = FetchType.EAGER)
    private List<Review> reviewList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "item")
    private List<OrderItem> orderItems;

    public Item() {
        reviewList = new ArrayList<>();
        orderItems = new ArrayList<>();
        categoryList = new ArrayList<>();
    }

    public Item(int itemId, String itemType) {
        this.itemId = itemId;
        this.itemType = itemType;
    }

    public Item(int itemId, double listPrice, double costPrice, String name, Date dateAdded) {
        this.itemId = itemId;
        this.listPrice = listPrice;
        this.costPrice = costPrice;
        this.name = name;
        this.dateAdded = dateAdded;
    }

    public List<String> getListOf5() {
        List<String> i = new ArrayList<>();
        i.add("1");
        i.add("2");
        i.add("3");
        i.add("4");
        i.add("5");

        return i;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(double costPrice) {
        this.costPrice = costPrice;
    }

    public double getSalePrice() {
        return salesPrice;
    }

    public void setSalePrice(double sp) {
        this.salesPrice = sp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicturePath() {
        if(picturePath == null){
            picturePath  = "default.png";
        }
        
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        if(picturePath == null){
            this.picturePath  = "default.png";
        }
        
        this.picturePath = picturePath;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateRemoved() {
        return dateRemoved;
    }

    public void setDateRemoved(Date dateRemoved) {
        this.dateRemoved = dateRemoved;
    }

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public int getAverageReview() {
        double total = 0;
        int count = 0;
        for (Review r : getReviewList()) {
            total += (r.getStarRating() * 1.0);
            count++;
        }

        return (int) Math.round(total / count);
    }

    public int getReviewQuantity() {
        return getReviewList().size();
    }

    public void addReview(Review review) {
        reviewList.add(review);
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public void addOrderItem(OrderItem orderItem) {
        this.orderItems.add(orderItem);
    }

    public int countItem() {
        return this.getOrderItems().size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Item item = (Item) o;
        
        if(!item.name.equals(name)){
            return false;
        }
        
        if(!item.itemType.equals(itemType)){
           return false; 
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId);
    }

    @Override
    public String toString() {
        return "Item{"
                + "itemId=" + itemId
                + "itemType=" + itemType
                + ", listPrice=" + listPrice
                + ", costPrice=" + costPrice
                + ", salesPrice=" + salesPrice
                + ", name='" + name + '\''
                + ", picturePath='" + picturePath + '\''
                + ", dateAdded=" + dateAdded
                + ", dateRemoved=" + dateRemoved
                + ", categoryList=" + categoryList
                + ", reviewList=" + reviewList
                + '}';
    }
}
