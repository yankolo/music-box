/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import com.javaproject.musicbox.services.admin.ReportsService;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.inject.Inject;

/**
 * Entity class representing an user
 */
@Entity
@Table(name = "user")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @Column(name = "fname")
    private String firstName;
    @Column(name = "lname")
    private String lastName;
    private String company ="";
    @Column(name = "address_1")
    private String address1;
    @Column(name = "address_2")
    private String address2 = "";
    private String city;
    private String province;
    @Column(name = "home_phone")
    private String homePhone;
    @Column(name = "cell_phone")
    private String mobilePhone ="";
    @Column(name = "postal_code")
    private String postalCode;
    private String email;
    private byte[] hash;
    private String salt;
    @Column(name = "is_manager")
    private boolean isManager;


    @JoinColumn(name = "last_visited_category", referencedColumnName = "category_id")
    @OneToOne
    private Category lastVisitedCategory;

    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @OneToMany(cascade = CascadeType.PERSIST)
    private List<Answer> answers;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    private List<Order> orders;

    @OneToOne(mappedBy = "user", cascade = CascadeType.PERSIST)
    private ShoppingCart shoppingCart;

    @Transient
    private String password;

    @Transient
    private double totalPurchases;

    public User() {
    }

    public User(int userId) {
        this.userId = userId;
    }

    public User(String firstName, String lastName, String company, String address1, String address2, String city, String province, String homePhone, String mobilePhone, String postalCode, String email, byte[] hash, String salt, Category lastVisitedCategory, List<Answer> answers, List<Order> orders, String password, double totalPurchases) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.province = province;
        this.homePhone = homePhone;
        this.mobilePhone = mobilePhone;
        this.postalCode = postalCode;
        this.email = email;
        this.hash = hash;
        this.salt = salt;
        this.lastVisitedCategory = lastVisitedCategory;
        this.answers = answers;
        this.orders = orders;
        this.password = password;
        this.totalPurchases = totalPurchases;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode.trim().replaceAll(" ", "");
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Category getLastVisitedCategory() {
        return lastVisitedCategory;
    }

    public void setLastVisitedCategory(Category lastVisitedCategory) {
        this.lastVisitedCategory = lastVisitedCategory;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswer(List<Answer> answer) {
        this.answers = answer;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
    
     /**
     * Get the total amount of money the user payed for all the items per user
     *
     * @return
     */
    public double getTotalUserPayed() {
        double sum = 0 ;
        for(Order o : this.orders){
            //for each order add the sum
            sum+= o.getGrandTotal();
                
            }
        return Math.round(sum * 100.0) / 100.0;
        }
   
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", company='" + company + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", province='" + province + '\'' +
                ", homePhone='" + homePhone + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", email='" + email + '\'' +
                ", hash=" + Arrays.toString(hash) +
                ", salt='" + salt + '\'' +
                ", lastVisitedCategory=" + lastVisitedCategory +
                ", answer=" + answers +
                ", orders=" + orders +
                ", password='" + password + '\'' +
                ", isManager='" + isManager + '\'' +
                ", totalPurchases='" + totalPurchases + '\'' +
                '}';
    }

    public double getTotalPurchases() {
        return totalPurchases;
    }

    public void setTotalPurchases(double totalPurchases) {
        this.totalPurchases = totalPurchases;
    }
}
