/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Yanik
 */
@Embeddable
public class OrderItemPK implements Serializable {

    @Column(name = "item_id")
    private int itemId;

    @Column(name = "order_id")
    private int orderId;

    public OrderItemPK() {
    }

    public OrderItemPK(int itemId, int orderId) {
        this.itemId = itemId;
        this.orderId = orderId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItemPK that = (OrderItemPK) o;
        return itemId == that.itemId &&
                orderId == that.orderId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, orderId);
    }

    @Override
    public String toString() {
        return "OrderItemPK{" +
                "itemId=" + itemId +
                ", orderId=" + orderId +
                '}';
    }
}
