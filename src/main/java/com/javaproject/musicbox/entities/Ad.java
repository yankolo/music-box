package com.javaproject.musicbox.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Entity class representing an Ad. It contains information related to the advertiser and whether it is actively
 * displayed on the home page.
 */
@Entity
@Table(name = "ads")
public class Ad implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ad_id")
    private int id;
    @Column(name = "image_url")
    private String imageURL;
    @Column(name = "url")
    private String websiteURL;
    @Column(name = "active_as_ad_1")
    private boolean isActiveAsAd1;
    @Column(name = "active_as_ad_2")
    private boolean isActiveAsAd2;

    public Ad(){}

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public boolean isActiveAsAd1() {
        return isActiveAsAd1;
    }

    public void setActiveAsAd1(boolean activeAsAd1) {
        isActiveAsAd1 = activeAsAd1;
    }

    public boolean isActiveAsAd2() {
        return isActiveAsAd2;
    }

    public void setActiveAsAd2(boolean activeAsAd2) {
        isActiveAsAd2 = activeAsAd2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ad ad = (Ad) o;
        return id == ad.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
