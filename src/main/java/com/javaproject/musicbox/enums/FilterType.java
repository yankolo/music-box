package com.javaproject.musicbox.enums;

/**
 * ENUM for all the types inheriting the item abstract type
 * 
 * @author Danny
 */
public enum FilterType {
     TRACK, ALBUM
}
