package com.javaproject.musicbox.enums;

/**
 *  ENUMS all filtering abilities
 * 
 * @author Danny
 */
public enum SearchFilter {
        ARTIST, LABEL, GENRE, TITLE, DATES
}
