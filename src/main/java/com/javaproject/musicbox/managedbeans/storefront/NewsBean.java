/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.News;
import com.javaproject.musicbox.services.NewsService;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class NewsBean {
    
    @Inject
    private NewsService newsService;
    
    public News getNews(){
        return newsService.getActivatedNews();
    }
    
}
