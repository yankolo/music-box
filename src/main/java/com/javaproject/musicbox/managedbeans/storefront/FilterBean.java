/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.enums.FilterType;
import com.javaproject.musicbox.enums.SearchFilter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@SessionScoped
public class FilterBean implements Serializable {

    private List<FilterType> filterTypeList;
    private List<SearchFilter> searchFilterList;
    private Date from;
    private Date to;

    @Inject
    public void init(){
        //defaults
        if (filterTypeList == null) {
            filterTypeList = new ArrayList<>();
            filterTypeList.add(FilterType.TRACK);
            filterTypeList.add(FilterType.ALBUM);
        }
        
        //defaults
        if (searchFilterList == null) {
            searchFilterList = new ArrayList<>();
            searchFilterList.add(SearchFilter.ARTIST);
            searchFilterList.add(SearchFilter.LABEL);
            searchFilterList.add(SearchFilter.GENRE);
            searchFilterList.add(SearchFilter.TITLE);
        }
        
        to = new Date();
        from = new Date();
    }
    
    public List<FilterType> getFilterTypeList() {
        return filterTypeList;
    }

    public void setFilterTypeList(List<FilterType> filterTypeList) {
        this.filterTypeList = filterTypeList;
    }

    public List<SearchFilter> getSearchFilterList() {
        return searchFilterList;
    }

    public void setSearchFilterList(List<SearchFilter> searchFilterList) {

        this.searchFilterList = searchFilterList;
    }
    
    
    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

}
