/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.enums.FilterType;
import com.javaproject.musicbox.enums.SearchFilter;
import com.javaproject.musicbox.services.ItemService;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * ManagedBean for querying and display item information
 *
 * @author Danny
 */
@Named
@RequestScoped
public class CatalogBean implements Serializable {
    @Inject
    ItemService itemService;
    @Inject
    SearchBean searchBean;

    private List<Item> items;
    private SortOption currentSortOption;
    private List<FilterType> ft;
    private List<SearchFilter> sf;

    public enum SortOption {
        NAME("standard", "A to Z", (a1, a2) -> a1.getName().compareToIgnoreCase(a2.getName())),
        NAME_REVERSE("reverse", "Z to A", (a1, a2) -> (a1.getName().compareToIgnoreCase(a2.getName())) * -1),
        REVIEW("review", "By Review", (a1, a2) -> 0); // Not implemented yet

        private final String value;
        private final String label;
        private final Comparator<Item> comparator;

        SortOption(String value, String label, Comparator<Item> comparator) {
            this.value = value;
            this.label = label;
            this.comparator = comparator;
        }

        public String getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    /**
     * Instantiates necessary components
     */
    @PostConstruct
    private void init() {
        if (searchBean.getItemsMatchingSearch() != null) {
            items = searchBean.getItemsMatchingSearch();
        } else {
            items = itemService.findAll();
        }
        
        ft = new ArrayList<>();
        ft.add(FilterType.TRACK);
        ft.add(FilterType.ALBUM);
        
        sf = new ArrayList<>();
        sf.add(SearchFilter.TITLE);
        sf.add(SearchFilter.LABEL);
        sf.add(SearchFilter.ARTIST);
        sf.add(SearchFilter.GENRE);
        sf.add(SearchFilter.DATES);
        

        currentSortOption = getSortOptions()[0];
        items.sort(currentSortOption.comparator);
    }

    /**
     * Select's element handler for changing value
     * Redefines which algorithm to use for sorting
     *
     * @param event
     */
    public void sortByHandler(ValueChangeEvent event) {
        String sortValue = (String) event.getNewValue();
        for (SortOption sortOption : getSortOptions()) {
            if (sortValue.equals(sortOption.value)) {
                currentSortOption = sortOption;
                items.sort(currentSortOption.comparator);
                break;
            }
        }
    }


    public List<FilterType> getFt() {
        return ft;
    }

    public void setFt(List<FilterType> ft) {
        this.ft = ft;
    }

    public List<SearchFilter> getSf() {
        return sf;
    }

    public void setSf(List<SearchFilter> sf) {
        this.sf = sf;
    }

    public List<Item> getItems() {
        return items;
    }

    public SortOption[] getSortOptions() {
        return SortOption.values();
    }
}
