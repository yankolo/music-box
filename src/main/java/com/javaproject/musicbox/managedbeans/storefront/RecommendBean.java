/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.ItemService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @author Danny
 */
@Named
@RequestScoped
public class RecommendBean {
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    private ItemService itemService;

    /**
     * Confirms the presence of a last visited category from the logged in user
     *
     * @return boolean
     */
    public boolean hasRecommendation() {
        boolean hasRecommend = false;

        if (user != null) {
            hasRecommend = user.getLastVisitedCategory() != null;
        }

        return hasRecommend;
    }

    /**
     * Returns according to the state of the application
     *
     * @return List<Item>
     */
    public List<Item> getList() {
        if (hasRecommendation()) {
            return getRecommend();
        } else {
            return getLatest();
        }
    }

    private List<Item> getLatest() {
        return itemService.findLatest(4);
    }

    private List<Item> getRecommend() {
        return itemService.findRecommend(4, user);
    }

}
