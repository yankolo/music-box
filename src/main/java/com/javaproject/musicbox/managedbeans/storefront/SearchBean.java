/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.services.ItemService;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * The SearchBean is responsible contains the search input and the
 * items that match the search input
 *
 * @author Danny
 */
@Named
@SessionScoped
public class SearchBean implements Serializable {
    @Inject
    private ItemService itemService;
    
    @Inject
    private FilterBean filterBean;

    private String searchTerm;
    private List<Item> itemsMatchingSearch;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public List<Item> getItemsMatchingSearch() {
        return itemsMatchingSearch;
    }

    public String searchForItems() {
        itemsMatchingSearch = itemService.findByCriteria(searchTerm, filterBean.getFilterTypeList(), filterBean.getSearchFilterList(), filterBean.getFrom(), filterBean.getTo());
        
        if(itemsMatchingSearch.size() == 1){
            return "/storefront/pages/public/itemDetails.xhtml?id=" + itemsMatchingSearch.get(0).getItemId();
        }
        
        return null;
    }
}
