package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;

/**
 * User registration managed bean
 */
@Named
@RequestScoped
public class RegistrationBean {
    @Inject
    private UserService userService;

    private User user;
    private ResourceBundle bundle;

    @PostConstruct
    public void init() {
        user = new User();
        bundle = ResourceBundle.getBundle("language");
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Validates that the email address is presumably valid (doesn't verify if the address exists)
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateEmail(FacesContext context, UIComponent component, Object value) {
        String email = (String) value;

        // First check if the email address might be valid using a regex pattern
        boolean isValidEmail = email.matches("^([a-zA-Z0-9_\\-.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$");
        if (!isValidEmail) {
            ((UIInput) component).setValid(false);
            FacesMessage msg = new FacesMessage(bundle.getString("invalidEmailErr"));
            context.addMessage(component.getClientId(context), msg);
        } else {
            // If the email is valid, check if the email is already used
            boolean emailUsed = userService.checkEmailUsed(email);
            if (emailUsed) {
                ((UIInput) component).setValid(false);
                FacesMessage msg = new FacesMessage(bundle.getString("usedEmailErr"));
                context.addMessage(component.getClientId(context), msg);
            }
        }
    }

    /**
     * Validates that the postal code is in a valid format
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePostalCode(FacesContext context, UIComponent component, Object value) {
        String postalCode = (String) value;

        // Check if the postal code is valid (+ with no spaces in-between) using a regex expression
        boolean isValidPostalCode = postalCode.trim().replaceAll(" ", "").matches("^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1}\\d{1}[A-Za-z]{1}\\d{1}$");
        if (!isValidPostalCode) {
            ((UIInput) component).setValid(false);
            FacesMessage msg = new FacesMessage(bundle.getString("invalidPostalCodeErr"));
            context.addMessage(component.getClientId(context), msg);
        }
    }

    /**
     * Validates & registers a new user and adds a new record to the users table in the database
     */
    public String registerUser() throws InvalidKeySpecException, NoSuchAlgorithmException {
        userService.registerUser(this.user);

        // Login user
        userService.loginUser(user.getEmail(), user.getPassword());

        return "/home?faces-redirect=true";
    }
}
