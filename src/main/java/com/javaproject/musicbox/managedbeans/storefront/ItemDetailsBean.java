/*
 * A managedbean that is responsible for showing Item information (will be
 * defined during runtime wether is a Track or an Album)
 * This managedbean will also display recommandations in case the Item is an Album
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.util.ContextUtilities;
import java.io.IOException;
import java.util.List;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

/**
 * @author Lara
 */
@Named
@ViewScoped
public class ItemDetailsBean implements Serializable {
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    ItemService itemService;
    @Inject
    UserService userService;

    private int itemId;
    private Item item;
    private List<Item> similarTracks;
    private List<Item> similarAlbums;

    /**
     * This method will find the item that is represented by the item id in the
     * query string. It will redirect to 404 page if item id in query string
     * doesn't exist
     * @throws java.io.IOException
     */
    public void findItem() throws IOException {
        if (itemId <= 0) {
            ContextUtilities.redirectTo404();
            return;
        }

        item = itemService.findById(itemId);

        if (item == null) {
            ContextUtilities.redirectTo404();
        } else {
            // query the database by
            // returning a list of 3 or less Items (Album or Tracks) that have the same category
            // as the currently viewed Item (Album or Track) by the user. 
            //The query will not include the current item.
            //Note that track page will display both.
            similarTracks = itemService.getAllItemsWithSameGenre(item.getCategoryList().get(0).getName(),
                                                                 item.getItemId(),
                                                                 "track");
            setCategory();
        }

        similarAlbums = itemService.getAllItemsWithSameGenre(item.getCategoryList().get(0).getName(),
                                                             item.getItemId(),
                                                             "album");
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public List<Item> getSimilarTracks() {
        return similarTracks;
    }

    public void setSimilarTracks(List<Item> similarItems) {
        this.similarTracks = similarItems;
    }

    public List<Item> getSimilarAlbums() {
        return similarAlbums;
    }

    public void setSimilarAlbums(List<Item> similarAlbums) {
        this.similarAlbums = similarAlbums;
    }

    /**
     * If user is logged in, it will save the main category of the item for the
     * user
     */
    public void setCategory() {
        if (user != null) {
            userService.setLastVisitedCategory(user, getMainCategory(item.getCategoryList()).getName());
        }
    }

    /**
     * Get main category if not null
     *
     * @param clist
     * @return
     */
    private Category getMainCategory(List<Category> clist) {
        if (clist != null) {
            return clist.get(0);
        }
        //Shouldn't be null
        return null;
    }
    
    public long countApproved(){
        return item.getReviewList().stream().filter(review -> review.getIsApproved() == true).count();
    }
}
