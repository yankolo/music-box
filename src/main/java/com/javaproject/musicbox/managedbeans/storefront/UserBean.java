/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * @author Danny
 */
@Named
@RequestScoped
public class UserBean implements Serializable {
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    private UserService userService;

    public boolean isLoggedIn() {
        return user != null;
    }

    public User getUser() {
        return user;
    }
}
