/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront.admin;

import com.javaproject.musicbox.entities.Album;
import com.javaproject.musicbox.entities.Artist;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.OrderItem;
import com.javaproject.musicbox.entities.Track;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.services.admin.ReportsService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Lara
 */
@Named
@RequestScoped
public class ReportsBean implements Serializable {

    @Inject
    ReportsService rs;
    @Inject
    UserService us;
    @Inject
    ItemService is;
    private List<Order> orders;
    private double totalSales;
    private Date from, to;
    private User user;
    private Artist artist;
    private Item track, album;
    private List<Item> distinctItems, topSellersTrack, topSellersAlbum, zeroSellers;
    private List<User> topSellerUsers, zeroSellerUsers;
    private String artistId, clientId, trackId, albumId;

    @PostConstruct
    private void init() {
        /**
         * This is temp due to a bug
         */

        artistId = "1";
        albumId = "1";
        trackId = "70";
        clientId = "2";
        artist = rs.getAllArtists().get(47 - 1);
        track = is.findById(70);
        album = is.findById(1);
        user = us.findById(2);
        /**
         * ************************************
         */
        from = new Date();
        to = new Date();
        //get all orders according to date
        orders = rs.getAllOrderedItems(from, to);
        //temp list
        List<Item> items = new ArrayList<>();
        //create a disctinct list of items that are in the order table
        orders.forEach(order -> order.getOrderItems().forEach(orderItem -> items.add(orderItem.getItem())));
        distinctItems = items.stream().distinct().collect(Collectors.toList());
        //total sales for the disctinct items from given dates
        totalSales = Math.round(rs.getTotalCost(new Date(), new Date()) * 100.0) / 100.0;
        //top track and album sellers
        topSellersTrack = distinctItems.stream().filter(item -> item.getItemType().equals("track")).sorted(Comparator.comparing(Item::countItem).reversed())
                .collect(Collectors.toList());
        topSellersAlbum = distinctItems.stream().filter(item -> item.getItemType().equals("album")).sorted(Comparator.comparing(Item::countItem).reversed())
                .collect(Collectors.toList());
        //filter zero sellers items
        zeroSellers = rs.getAllItems().stream().filter(item -> item.countItem() == 0).collect(Collectors.toList());
        //filter top buyers
        topSellerUsers = rs.getAllUsers().stream().filter(user -> user.getTotalUserPayed() > 0).sorted(Comparator.comparing(User::getTotalUserPayed).reversed()).collect(Collectors.toList());
        //filter zero items
        zeroSellerUsers = rs.getAllUsers().stream().filter(user -> user.getTotalUserPayed() == 0).sorted(Comparator.comparing(User::getTotalUserPayed).reversed()).collect(Collectors.toList());
    }

    public List<Item> getDistinctItems() {
        return distinctItems;
    }

    public void setDistinctItems(List<Item> distinctItems) {
        this.distinctItems = distinctItems;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    public List<User> getUsers() {
        return rs.getAllUsers();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Item getTrack() {
        return track;
    }

    public void setTrack(Item track) {
        this.track = track;
    }

    public Item getAlbum() {
        return album;
    }

    public void setAlbum(Item album) {
        this.album = album;
    }

    public List<Order> ordersPerAlbum() {
        return rs.getOrdersForItem(album.getItemId());
    }

    public List<Item> getTopSellersTrack() {
        return topSellersTrack;
    }

    public void setTopSellersTrack(List<Item> topSellersTrack) {
        this.topSellersTrack = topSellersTrack;
    }

    public List<Item> getTopSellersAlbum() {
        return topSellersAlbum;
    }

    public void setTopSellersAlbum(List<Item> topSellersAlbum) {
        this.topSellersAlbum = topSellersAlbum;
    }

    public List<Item> getZeroSellers() {
        return zeroSellers;
    }

    public void setZeroSellers(List<Item> zeroSellers) {
        this.zeroSellers = zeroSellers;
    }

    public List<User> getTopSellerUsers() {
        return topSellerUsers;
    }

    public void setTopSellerUsers(List<User> topSellerUsers) {
        this.topSellerUsers = topSellerUsers;
    }

    public List<User> getZeroSellerUsers() {
        return zeroSellerUsers;
    }

    public void setZeroSellerUsers(List<User> zeroSellerUsers) {
        this.zeroSellerUsers = zeroSellerUsers;
    }

    public String getArtistId() {
        return artistId;
    }

    public void setArtistId(String artistId) {
        this.artistId = artistId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    /**
     * Get all the items the user purchased
     *
     * @return
     */
    public List<Item> getUserItems() {
        return rs.getAllUsersItem(user);
    }

    /**
     * Get the total amount of money the user payed for all the items they
     * ordered
     *
     * @return
     */
    public double getTotalUserPayed() {
        return Math.round(rs.getTotalUserPayed(user) * 100.0) / 100.0;
    }

    /**
     * Get all artists
     *
     * @return
     */
    public List<Artist> getArtists() {
        return rs.getAllArtists();
    }

    /**
     * Count how many times the items was ordered
     *
     * @param i
     * @return
     */
    public int countItem(Item i) {
        return i.getOrderItems().size();
    }

    /**
     * Get all items within a given type
     *
     * @param type
     * @return
     */
    public List<Item> getAllItems(String type) {
        return rs.getAllItemsWithType(type);
    }

    /**
     * Get total sales per album
     *
     * @return
     */
    public double totalSalesPerAlbum() {
        return Math.round(countItem(album) * (album.getListPrice() - album.getSalePrice()) * 100.0) / 100.0;
    }

    /**
     * Get total sales per track
     *
     * @return
     */
    public double totalSalesPerTrack() {
        return Math.round(countItem(track) * (track.getListPrice() - track.getSalePrice()) * 100.0) / 100.0;
    }

    /**
     * Get all orders that contain a given album
     *
     * @return
     */
    public List<OrderItem> getAllTrackOrders() {
        return is.findById(track.getItemId()).getOrderItems();
    }

    /**
     * Get all orders that contain a given track
     *
     * @return
     */
    public List<OrderItem> getAllAlbumOrders() {
        return is.findById(album.getItemId()).getOrderItems();
    }

    /**
     * Calculate the total money gained by an artist from albums
     *
     * @return
     */
    public double totalPayedArtistAlbum() {
        double gain = 0;
        //all the albums of the artist that were sold 
        for (Album a : artist.getAlbumCollection()) {
            gain += a.countItem() * a.getListPrice() - a.getSalePrice(); //to add
        }
        return gain;
    }

    /**
     * Calculate the total money gained by an artist from track
     *
     * @return
     */
    public double totalPayedArtistTrack() {
        double gain = 0;
        //all the albums of the artist that were sold 
        for (Track a : artist.getTrackCollection()) {
            gain += a.countItem() * a.getListPrice() - a.getSalePrice(); //to add
        }
        return gain;
    }

    public void submitArtist() {
        artist = rs.getAllArtists().get(Integer.parseInt(artistId) - 1);
    }

    public void submitClient() {
        user = us.findById(Integer.parseInt(clientId));
    }

    public void submitTrack() {
        track = is.findById(Integer.parseInt(trackId));
    }

    public void submitAlbum() {
        album = is.findById(Integer.parseInt(albumId));
    }

    public void filterByDate() {
      
    }
}
