/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.services.ItemService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class SalesBean {
    
    @Inject 
    private ItemService itemService;
    
    private List<Item> list;
    
    
    @PostConstruct
    public void init(){
        list = itemService.findSpecials(4);
    }
    
    
    public List<Item> getList(){
        return list;
    }
    
}
