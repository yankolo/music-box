package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.*;
import com.javaproject.musicbox.jobs.EmailSender;
import com.javaproject.musicbox.services.OrderService;
import com.javaproject.musicbox.services.ShoppingCartService;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.util.ContextUtilities;
import com.javaproject.musicbox.util.PurchaseUtilities;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.flow.FlowScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Named
@RequestScoped
public class CheckoutBean implements Serializable {
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    private UserService userService;
    @Inject
    private OrderService orderService;
    @Inject
    private ShoppingCartService shoppingCartService;
    @Inject
    private EmailSender emailSender;

    private Order order;
    private boolean isOrderComplete = false;

    /**
     * Prepare the order object which will need to be persisted
     * when an order is placed
     * <p>
     * An external change to the shopping cart could lead to potentially
     * unwanted behavior the checkout process (e.g. in the checkout process,
     * if the user updates his shopping cart on a different computer
     * and then clicks on "checkout" in his initial computer the user
     * will pay for items that were not shown in checkout page).
     * For this reason, it is better to tie the checkout process (i.e. the
     * CheckoutBean) to an order which has not yet been placed yet instead of
     * representing the items in the checkout process using the current shopping cart.
     */
    @PostConstruct
    private void init() {
        order = new Order(user, new Date());

        List<Item> itemsToBuy = user.getShoppingCart().getItems();

        // Creating a list of order items to be able to put the items inside the order
        List<OrderItem> orderItems = new ArrayList<>();
        for (Item item : itemsToBuy) {
            OrderItem orderItem = new OrderItem();
            orderItem.setOrder(order);
            orderItem.setItem(item);

            if (item.getSalePrice() == 0) {
                orderItem.setPrice(item.getListPrice());
            } else {
                orderItem.setPrice(item.getSalePrice());
            }

            orderItems.add(orderItem);
        }
        order.setOrderItems(orderItems);

        // Setting the user of the order
        order.setUser(user);

        // Setting the totals of the order
        double subtotal = PurchaseUtilities.calcSumOfPrices(itemsToBuy.toArray(new Item[0]));
        double taxes = PurchaseUtilities.calcTaxes(subtotal);
        double grandTotal = PurchaseUtilities.calcGrandTotal(subtotal, taxes);

        order.setSubtotal(subtotal);
        order.setTaxes(taxes);
        order.setGrandTotal(grandTotal);
    }

    /**
     * Places an order and clears the user's shopping cart
     */
    public String placeOrder() throws MessagingException, UnsupportedEncodingException {
        // Adding the order to the user
        user.getOrders().add(order);

        ShoppingCart userShoppingCart = user.getShoppingCart();
        userShoppingCart.getItems().clear();
        shoppingCartService.update(userShoppingCart);

        orderService.create(order);
        userService.update(user);
        isOrderComplete = true;

        emailSender.sendOrderConfirmation(order);

        return "/storefront/pages/authenticated/orderConfirmation";
    }

    public void redirectToCart() throws IOException {
        ContextUtilities.redirectTo("/storefront/pages/public/cart.xhtml");
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public boolean isOrderComplete() {
        return isOrderComplete;
    }

    public void setOrderComplete(boolean orderComplete) {
        isOrderComplete = orderComplete;
    }
}
