package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.util.SessionUtilities;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * Managed bean for authenticating/logging in an user
 */
@Named
@RequestScoped
public class LoginBean {
    // Fields used to authenticate an user
    private String email;
    private String password;
    private boolean validLogin = true;

    @Inject
    private UserService userService;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isValidLogin() {
        return validLogin;
    }

    public void setValidLogin(boolean validLogin) {
        this.validLogin = validLogin;
    }

    /**
     * Authenticate an user - checks if email and password matches a valid entry in the database
     *
     * @return
     */
    public String authenticate() throws InvalidKeySpecException, NoSuchAlgorithmException {
        // Call the UserServices's login method to login the user
        boolean isAuthenticated = userService.loginUser(email, password);
        if (!isAuthenticated) {
            validLogin = false;
            return "";
        }

        //Get the logged in user instance
        User user = userService.getCurrentUser();
        //Redirect the user to the manager site if they are a manager
        if (user.isManager()) {
            return "/storefront/pages/admin/reports?faces-redirect=true";
        }
        return "/home?faces-redirect=true";
    }

    /**
     * Logout the current user by using the appropriate SessionUtilities method.
     *
     * @return the outcome of the action (redirect to website home page)
     */
    public String logout() {
        SessionUtilities.logoutSessionUser();
        return "/home?faces-redirect=true";
    }
}
