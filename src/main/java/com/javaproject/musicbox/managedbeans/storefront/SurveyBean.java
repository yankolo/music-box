package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Answer;
import com.javaproject.musicbox.services.SurveyService;
import com.javaproject.musicbox.util.SessionUtilities;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named("surveyBean")
@RequestScoped
public class SurveyBean implements Serializable {
    private String question;
    private List<String> options;
    private int answer;
    private List<Long> resultsPercentages;
    private long totalVotes;
    private boolean showSurvey;

    @Inject
    private SurveyService surveyDAO;

    @PostConstruct
    public void init(){
        this.question = surveyDAO.getSurveyQuestion();
        this.options = surveyDAO.getSurveyOptions();
        this.totalVotes = surveyDAO.getTotalVotes();
        setPreviousAnswer(); //Set the value of the user's saved/previously recorded answer if there is one

        //If the user is a manager, calculate the results if there are votes
        if(SessionUtilities.getSessionUser().isManager() && totalVotes != 0){
            calculateResultPercentages();
        }
        //User hasn't answered survey yet
        else if(answer == 0){
            showSurvey = true;
        }
        //User has already answered the survey -> calculate the results
        else{
           showSurvey = false;
            calculateResultPercentages();
        }
    }

    /**
     * Calls the Survey DAO's submit method to save the user's answer to the database.
     */
    public void submit(){
        surveyDAO.submitSurveyAnswer(answer);
        //Recalculate results percentage
        calculateResultPercentages();
        //Synchronize the total votes with the DB
        this.totalVotes = surveyDAO.getTotalVotes();
        //Hide the survey and show the results
        this.showSurvey = false;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    /**
     * Set the default value of the survey to the previous answer if there is any.
     */
    public void setPreviousAnswer(){
        Answer previousAnswer = surveyDAO.getUserAnswer();
        if(previousAnswer != null){
            this.answer = previousAnswer.getAnswer();
        }
    }

    /**
     * Set the value of the results percentages so they can be displayed on the home page.
     */
    public void calculateResultPercentages(){
        resultsPercentages = new ArrayList<>();
        List<Long> numVotes = surveyDAO.getNumberOfVotes();
        long totalVotes = surveyDAO.getTotalVotes();
        for(long votes : numVotes){
            if(totalVotes != 0) {
                resultsPercentages.add((votes * 100) / totalVotes);
            }
            //Avoid division by zero exception
            else{
                resultsPercentages.add((long)0);
            }
        }
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public List<Long> getResultsPercentages() {
        return resultsPercentages;
    }

    public void setResultsPercentages(List<Long> resultsPercentages) {
        this.resultsPercentages = resultsPercentages;
    }

    public long getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(long totalVotes) {
        this.totalVotes = totalVotes;
    }

    public boolean isShowSurvey() {
        return showSurvey;
    }

    public void setShowSurvey(boolean showSurvey) {
        this.showSurvey = showSurvey;
    }
}
