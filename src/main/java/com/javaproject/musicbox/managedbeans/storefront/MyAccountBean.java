package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.util.EncryptionUtilities;
import com.javaproject.musicbox.util.SessionUtilities;
import org.omnifaces.util.Faces;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Named
@RequestScoped
public class MyAccountBean {
    @LoggedInUser
    @Inject
    private User user;

    private String currentPassword;
    private String newPassword;

    @Inject
    private UserService userService;

    /**
     * Method to download a sample song
     * @throws IOException
     */
    public void downloadTrack() throws IOException {
        String fileName = "SampleSong.mp3";
        File file = new File(getClass().getClassLoader().getResource(fileName).getFile());

        Faces.sendFile(file, true);
    }

    /**
     * Method to change the password
     */
    public String changePassword() throws InvalidKeySpecException, NoSuchAlgorithmException {
        userService.changePassword(user, newPassword);

        FacesContext.getCurrentInstance().addMessage(null, new javax.faces.application.FacesMessage("Your password has been changed"));

        return null;
    }

    /**
     * Validates the current password to verify if it is indeed the current password
     */
    public void validateCurrentPassword(FacesContext context, UIComponent component, Object value) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String password = (String) value;

        if (!EncryptionUtilities.passwordMatches(password, user.getHash(), user.getSalt())) {
            ((UIInput) component).setValid(false);
            FacesMessage msg = new FacesMessage("Password does not match the current password");
            context.addMessage(component.getClientId(context), msg);
        }
    }

    /**
     * Logs out the user
     */
    public String logout() {
        SessionUtilities.removeSessionUser();

        return "/home?faces-redirect=true";
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
