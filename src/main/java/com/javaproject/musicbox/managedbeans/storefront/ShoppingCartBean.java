package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.CurrentCart;
import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.ShoppingCart;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.ShoppingCartService;
import com.javaproject.musicbox.util.PurchaseUtilities;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class ShoppingCartBean implements Serializable {
    @CurrentCart
    @Inject
    private ShoppingCart shoppingCart;
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    private ShoppingCartService shoppingCartService;
    @Inject
    private ItemService itemDAO;

    private double subtotal;
    private double taxes;
    private double grandTotal;

    @PostConstruct
    private void init() {
        updateCartTotals();
    }

    /**
     * This addItemToCart() method adds an item to the cart
     *
     * @param itemId The id of the item to add
     */
    public void addItemToCart(int itemId) {
        // If the user is a manager, do not let them add to cart
        // The website will therefore be in 'Preview mode'
        // Meaning that managers can view the changes they made to
        // The website but not actually use it.
        if(user != null && user.isManager()){
            return;
        }
        Item itemAdded = itemDAO.findById(itemId);

        // If the item already exists in the cart, prevent adding it
        // Since in our case it doesn't make sense to have the same item
        // more than once in the cart
        if (!getItems().contains(itemAdded)) {
            shoppingCartService.addItemToCurrentCart(itemAdded);
        }

        updateCartTotals();
    }

    /**
     * This removeItemFromCart() method removes an item from the cart
     *
     * @param itemId The id of the item to remove
     */
    public void removeItemFromCart(int itemId) {
        Item itemToRemove = itemDAO.findById(itemId);

        shoppingCartService.removeItemFromCurrentCart(itemToRemove);

        updateCartTotals();
    }

    /**
     * The isCartEmpty() method verifies if the cart is empty
     *
     * @return True if the shopping cart is true
     */
    public boolean isCartEmpty() {
        return shoppingCart.getItems().size() == 0;
    }

    /**
     * Update the totals of the cart (i.e. subtotal, taxes, and grand total)
     */
    private void updateCartTotals() {
        subtotal = PurchaseUtilities.calcSumOfPrices(shoppingCart.getItems().toArray(new Item[0]));
        taxes = PurchaseUtilities.calcTaxes(subtotal);
        grandTotal = PurchaseUtilities.calcGrandTotal(subtotal, taxes);
    }

    public List<Item> getItems() {
        return shoppingCart.getItems();
    }

    public int getCount() {
        return shoppingCart.getItems().size();
    }

    public double getSubtotal() {
        return subtotal;
    }

    public double getTaxes() {
        return taxes;
    }

    public double getGrandTotal() {
        return grandTotal;
    }
}
