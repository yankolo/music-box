/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.services.CategoryService;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Danny
 */

@Named
@RequestScoped
public class CategoryBean {
    @Inject
    private CategoryService categoryService;

    public List<Category> getAllCategory() {
        return categoryService.findAll();
    }
}
