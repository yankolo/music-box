package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.entities.Ad;
import com.javaproject.musicbox.services.AdService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class AdBean implements Serializable {
    private Ad leftAd;
    private Ad rightAd;

    @Inject
    private AdService adDAO;

    @PostConstruct
    public void init(){
        leftAd = adDAO.getActiveAd(1);
        rightAd = adDAO.getActiveAd(2);
    }

    public Ad getLeftAd() {
        return leftAd;
    }

    public void setLeftAd(Ad leftAd) {
        this.leftAd = leftAd;
    }

    public Ad getRightAd() {
        return rightAd;
    }

    public void setRightAd(Ad rightAd) {
        this.rightAd = rightAd;
    }
}
