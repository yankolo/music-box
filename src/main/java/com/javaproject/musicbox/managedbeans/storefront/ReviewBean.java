/*
 * A managedbean that is responsible for storing and
 * viewing reviews of a specific Item (Album or Track)
 */
package com.javaproject.musicbox.managedbeans.storefront;

import com.javaproject.musicbox.annotations.LoggedInUser;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Review;
import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.ReviewService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

/**
 * @author Lara
 */
@Named
@RequestScoped
public class ReviewBean {
    @LoggedInUser
    @Inject
    private User user;
    @Inject
    private ReviewService reviewService;
    @Inject
    private ItemService itemService;

    private String comment;
    private int star;
    private boolean successfullySubmitted;

    /**
     * Adds a review to the db
     *
     * @return
     */
    public void addReview(int itemId) {
        // Verify if user logged in since only logged in users have this privilege
        if (user != null) {
            Item item = itemService.findById(itemId);
            Review newReview = new Review(new Date(), star, false, user, item, comment);

            item.addReview(newReview);
            itemService.update(item);

            //display succesful messgae
            successfullySubmitted = true;
        }
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isSuccessfullySubmitted() {
        return successfullySubmitted;
    }

    public void setSuccessfullySubmitted(boolean successfullySubmitted) {
        this.successfullySubmitted = successfullySubmitted;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
