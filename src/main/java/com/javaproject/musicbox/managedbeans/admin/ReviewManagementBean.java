package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Review;
import com.javaproject.musicbox.services.ReviewService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Lara
 */
@Named
@RequestScoped
public class ReviewManagementBean {

    @Inject
    ReviewService rs;
    private List<Review> unapprovedReviews;
    private List<Review> approvedReviews;

    @PostConstruct
    public void init() {
        unapprovedReviews = rs.getAllReviewsByCriteria(false);
        approvedReviews = rs.getAllReviewsByCriteria(true);
    }
    public List<Review> getUnapprovedReviews() {
        return unapprovedReviews;
    }

    public void setUnapprovedReviews(List<Review> unapprovedReviews) {
        this.unapprovedReviews = unapprovedReviews;
    }

    public List<Review> getApprovedReviews() {
        return approvedReviews;
    }

    public void setApprovedReviews(List<Review> approvedReviews) {
        this.approvedReviews = approvedReviews;
    }
}