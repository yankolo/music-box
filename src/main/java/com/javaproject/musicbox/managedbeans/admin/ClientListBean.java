package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class ClientListBean {
    @Inject
    private UserService userDAO;

    private List<User> users;

    @PostConstruct
    private void init(){
        users = userDAO.findAll();
        for(User user : users){
            user.setTotalPurchases(calculateTotalPurchasesValue(user.getUserId()));
        }
    }

    /**
     * Calls the User Service's getTotalUserPurchases method.
     * @param userID the client whose purchase total we want
     * @return the total value in $ of client's purchases
     */
    public double calculateTotalPurchasesValue(int userID){
        return userDAO.getTotalUserPurchases(userID);
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
