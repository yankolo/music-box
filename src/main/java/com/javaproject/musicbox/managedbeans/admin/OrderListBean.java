package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.services.OrderService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class OrderListBean {
    @Inject
    private OrderService orderService;

    private List<Order> orders;

    @PostConstruct
    private void init() {
        orders = orderService.findAll();
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
