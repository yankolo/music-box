package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.services.SurveyService;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class SurveyEditorBean implements Serializable {
    @Inject
    private SurveyService surveyDAO;

    private String question;
    private String option1;
    private String option2;
    private String option3;
    private String option4;

    /**
     * Creates a new survey from the input.
     * @return the outcome of the action (return to survey list page)
     */
    public String createNewSurvey(){
        //Wrap options in a list
        List<String> options = new ArrayList<>();
        options.add(option1);
        options.add(option2);
        options.add(option3);
        options.add(option4);

        //Call survey DAO create method
        surveyDAO.createSurvey(this.question, options);

        //Redirect back to survey list page
        return "/storefront/pages/admin/surveyList?faces-redirect=true";
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }
}
