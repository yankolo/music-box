package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.services.ArtistService;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class ArtistEditorBean {
    @Inject
    private ArtistService artistService;
    
    private String name;

    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String createArtist(){
        artistService.createArtist(name);
        
        return "/storefront/pages/admin/inventoryList.xhtml?faces-redirect=true&amp;editSuccess=true";
    }
    
}
