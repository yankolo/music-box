/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.services.NewsService;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@ViewScoped
public class NewsEditorBean implements Serializable{
    @Inject
    private NewsService newsService;
    
    private String url;
    
    public String createNews(){
        
        newsService.createNews(url);
        
        return "/storefront/pages/admin/adList?faces-redirect=true";
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    
}
