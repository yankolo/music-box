/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.services.ItemService;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class InventoryListBean {

    @Inject
    private ItemService itemService;

    private List<Item> inventory;

    private boolean success = false;

    @PostConstruct
    private void init() {
        inventory = itemService.findAll();
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public void setInventory(List<Item> inventory) {
        this.inventory = inventory;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Removes item by date removed
     */
    public void toggle(int itemId) {
        Item item = itemService.findById(itemId);
        if (item != null) {
            if (item.getDateRemoved() == null) {
                item.setDateRemoved(new Date());
            } else {
                item.setDateRemoved(null);
            }
            itemService.update(item);

            inventory = itemService.findAll();
        }
    }
}
