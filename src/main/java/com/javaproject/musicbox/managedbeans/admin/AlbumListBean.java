/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Album;
import com.javaproject.musicbox.services.ItemService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class AlbumListBean {
    @Inject
    private ItemService itemService;
    
    private List<Album> albumList;
    
    @PostConstruct
    public void init(){
        albumList = itemService.getAllAlbums();
    }

    public List<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }
    
}
