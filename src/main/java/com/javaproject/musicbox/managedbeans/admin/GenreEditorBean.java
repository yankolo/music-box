package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Category;
import com.javaproject.musicbox.services.CategoryService;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class GenreEditorBean {
    
    @Inject 
    private CategoryService categoryService;
    
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String createGenre(){
        categoryService.createCategory(name);
        
        return "/storefront/pages/admin/inventoryList.xhtml?faces-redirect=true&amp;editSuccess=true";
    }
}
