package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.OrderItem;
import com.javaproject.musicbox.entities.OrderItemPK;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.services.OrderService;
import com.javaproject.musicbox.util.ContextUtilities;
import com.javaproject.musicbox.util.PurchaseUtilities;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@ViewScoped
public class OrderEditorBean implements Serializable {
    @Inject
    private OrderService orderService;

    private int orderId;
    private Order order;

    private String userEmail;

    /**
     * By default, create an empty order which will be used to represent
     * the fields in the editor.
     */
    @PostConstruct
    private void init() {
        order = new Order();
    }

    /**
     * This method gets called if an order ID exists in the
     * URL query string
     */
    public void findOrder() throws IOException {
        if (orderId <= 0) {
            ContextUtilities.redirectTo404();
            return;
        }

        order = orderService.findById(orderId);

        if (order == null) {
            ContextUtilities.redirectTo404();
        } else {
            userEmail = order.getUser().getEmail();
        }
    }

    /**
     * Remove an item form the order (does not commit change)
     *
     */
    public void removeItemFromOrder(int orderId, int itemId) {
        OrderItem orderItem = orderService.findOrderItemByPK(orderId, itemId);
        order.removeOrderItem(orderItem);

        updateOrderTotals();
    }

    /**
     * Method to save the order
     */
    public void saveOrder() {
        orderService.update(order);
    }

    /**
     * Helper method to update the order totals
     */
    private void updateOrderTotals() {
        double subtotal = PurchaseUtilities.calcSumOfPrices(order.getOrderItems().toArray(new OrderItem[0]));
        double taxes = PurchaseUtilities.calcTaxes(subtotal);
        double grandTotal = PurchaseUtilities.calcGrandTotal(subtotal, taxes);

        order.setSubtotal(subtotal);
        order.setTaxes(taxes);
        order.setGrandTotal(grandTotal);
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
