/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Album;
import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.Track;
import com.javaproject.musicbox.services.ItemService;
import com.javaproject.musicbox.util.ContextUtilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 *
 * @author Danny
 */
@Named
@ViewScoped
public class InventoryEditorBean implements Serializable {

    @Inject
    private ItemService itemService;

    private int itemId;
    private Item item;
    private Part imageFile;
    private Album album;
    private Track track;
    private String itemType;
    private String fileAbsPath;

    /**
     * Sets default value to properties
     *
     */
    @PostConstruct
    private void init() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        fileAbsPath = ctx.getExternalContext().getInitParameter("image_source");

//        imageFile = new File(fileAbsPath + "default.png");
        item = new Album();
        album = (Album) item;
        itemType = "album";
        item.setItemType(itemType);

    }

    /**
     * Makes sure input file is a png or jpeg and check if size is too big
     *
     * @param ctx
     * @param comp
     * @param value
     */
    public void validateFile(FacesContext ctx, UIComponent comp, Object value) {
        List<FacesMessage> msgs = new ArrayList<FacesMessage>();
        Part file = (Part) value;

        if (file != null) {

            if (file.getSize() > 10000000) {
                msgs.add(new FacesMessage("File Too Big!"));
            }

            if (!"image/jpeg".equals(file.getContentType()) && !"image/png".equals(file.getContentType())) {
                msgs.add(new FacesMessage("Not a PNG or JPG"));
            }

            if (!msgs.isEmpty()) {
                throw new ValidatorException(msgs);
            }
        }

    }

    /**
     *
     * @return String
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets itemType string and sets the appropriate item type instance
     *
     * @param itemType
     */
    public void setItemType(String itemType) {
        if (itemType == null) {
            return;
        }

        this.itemType = itemType;
        if (itemType.equalsIgnoreCase("album")) {
            if (!(item instanceof Album)) {
                item = new Album();
                album = (Album) item;
                track = null;
            }
        } else {
            if (!(item instanceof Track)) {
                item = new Track();
                track = (Track) item;
                album = null;
            }
        }
        item.setItemType(itemType.toLowerCase());
    }

    /**
     *
     * @return ItemService
     */
    public ItemService getItemService() {
        return itemService;
    }

    /**
     *
     * @param itemService
     */
    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    /**
     *
     * @return
     */
    public int getItemId() {
        return itemId;
    }

    /**
     *
     * @param itemId
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     *
     * @return
     */
    public Item getItem() {
        return item;
    }

    /**
     *
     * @param item
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     *
     * @return
     */
    public Part getImageFile() {
        return imageFile;
    }

    /**
     *
     * @param imageFile
     */
    public void setImageFile(Part imageFile) {
        this.imageFile = imageFile;

        if (imageFile != null) {
            item.setPicturePath(imageFile.getSubmittedFileName());
            saveImage();
        }
    }

    /**
     *
     * @return
     */
    public Album getAlbum() {
        return album;
    }

    /**
     *
     * @param album
     */
    public void setAlbum(Album album) {
        this.album = album;
    }

    /**
     *
     * @return
     */
    public Track getTrack() {
        return track;
    }

    /**
     *
     * @param track
     */
    public void setTrack(Track track) {
        this.track = track;
    }

    /**
     * Finds item in the database for binding use
     *
     * @throws IOException
     */
    public void findItem() throws IOException {
        if (itemId <= 0) {
            ContextUtilities.redirectTo404();
            return;
        }

        item = itemService.findById(itemId);

        if (item == null) {
            ContextUtilities.redirectTo404();
        } else {
//            imageFile = new File(fileAbsPath + item.getPicturePath());
            itemType = item.getItemType();

            if (item instanceof Album) {
                album = (Album) item;
                track = null;

            } else if (item instanceof Track) {
                track = (Track) item;
                album = null;
            }
        }
    }

    /**
     * Remove Track Album
     */
    public void removeTrackAlbum() {
        if (track != null) {
            track.setAlbumId(null);
        }
    }

    /**
     * Saves item and return to inventoryList
     *
     * @return
     */
    public String saveItem() {
        try {
            if (item.getDateAdded() == null) {
                item.setDateAdded(new Date());
            }

            if (imageFile != null) {
                saveImage();
            }

            itemService.update(item);
            return "/storefront/pages/admin/inventoryList?faces-redirect=true&amp;editSuccess=true";
        } catch (Exception e) {
            return null;
        }
    }

    public void saveImage() {
        try (InputStream input = imageFile.getInputStream()) {
            File f = new File(fileAbsPath);
            File f2 = new File(fileAbsPath, imageFile.getSubmittedFileName());
            if (f.exists()) {
                Files.copy(input, f2.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            System.out.println("didnt work");
        }
    }

}
