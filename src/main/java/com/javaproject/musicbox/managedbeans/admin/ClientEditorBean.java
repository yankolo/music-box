package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.services.UserService;
import com.javaproject.musicbox.util.ContextUtilities;
import com.javaproject.musicbox.util.EncryptionUtilities;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@Named
@ViewScoped
public class ClientEditorBean implements Serializable {
    @Inject
    private UserService userDAO;

    private int userID;
    private User user;

    @PostConstruct
    private void init(){
        user = new User();
    }

    /**
     * Set the User instance of the current user whose registration info we want to edit. In the case where the user
     * is not found, the page redirects to 404.
     * @throws IOException
     */
    public void findUser() throws IOException {
        if(userID <= 0){
            ContextUtilities.redirectTo404();
            return;
        }

        user = userDAO.findById(userID);

        if(user == null){
            ContextUtilities.redirectTo404();
        }
    }

    /**
     * Saves the changes made to the user by calling the appropriate User Service method.
     * @return the outcome of the action (redirect back to client/user list)
     */
    public String saveUserInfo() throws InvalidKeySpecException, NoSuchAlgorithmException {
        //If a new password has been set for the user
        if(user.getPassword() != null && !user.getPassword().isEmpty()) {
            // Generate salt & hash from password
            String salt = EncryptionUtilities.generateSalt();
            byte[] hash = EncryptionUtilities.generateHash(user.getPassword(), salt);

            // Store the generated hash and salt in the user
            user.setHash(hash);
            user.setSalt(salt);
        }

        userDAO.update(user);

        //Redirect back to client/user list page
        return "/storefront/pages/admin/clientList?faces-redirect=true";
    }

    /**
     * Validates the email if the address has been modified by the manager only.
     * @param context Faces context
     * @param component the UIComponent to set as invalid
     * @param value the email address
     */
    public void validateEmail(FacesContext context, UIComponent component, Object value) {
        String email = (String) value;
        //First check if user email has been changed
        if(!email.equals(user.getEmail())) {

            // Check if the email address might be valid using a regex pattern
            boolean isValidEmail = email.matches("^([a-zA-Z0-9_\\-.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$");
            if (!isValidEmail) {
                ((UIInput) component).setValid(false);
                FacesMessage msg = new FacesMessage("Invalid email address");
                context.addMessage(component.getClientId(context), msg);
            } else {
                // If the email is valid, check if the email is already used
                boolean emailUsed = userDAO.checkEmailUsed(email);
                if (emailUsed) {
                    ((UIInput) component).setValid(false);
                    FacesMessage msg = new FacesMessage("Email is already in use");
                    context.addMessage(component.getClientId(context), msg);
                }
            }
        }
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
