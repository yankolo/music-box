/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.News;
import com.javaproject.musicbox.services.NewsService;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Danny
 */
@Named
@RequestScoped
public class NewsListBean {
    @Inject
    private NewsService newsDAO;
    
    private List<News> newsList;
    
    @PostConstruct
    public void init(){
        newsList = newsDAO.findAll();
    }
    
    public void activateNews(int id){
        newsDAO.setActivatedNews(id);
        
        newsList = newsDAO.findAll();
    }

    public List<News> getNewsList() {
        return newsList;
    }

    public void setNewsList(List<News> newsList) {
        this.newsList = newsList;
    }
}
