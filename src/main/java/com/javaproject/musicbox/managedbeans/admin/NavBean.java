package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.User;
import com.javaproject.musicbox.util.SessionUtilities;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class NavBean {
    private String activePage;
    private User currentUser;

    @PostConstruct
    private void init(){
        currentUser = SessionUtilities.getSessionUser();
    }

    public String getActivePage() {
        return activePage;
    }

    public void setActivePage(String activePage) {
        this.activePage = activePage;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
