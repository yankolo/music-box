package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Review;
import com.javaproject.musicbox.services.ReviewService;
import com.javaproject.musicbox.util.ContextUtilities;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Lara
 */
@Named
@ViewScoped
public class ReviewEditorBean implements Serializable {

    @Inject
    ReviewService rs;
    private Review review;
    private int id;

    @PostConstruct
    public void init() {
        review = new Review();
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public void findReview() throws IOException {
        if (id <= 0) {
            ContextUtilities.redirectTo404();
            return;
        }
        review = rs.findById(id);
    }

    /**
     * A method that changes the review approval to the opposite of what it is
     * currently
     */
    public void changeReviewStatus(ValueChangeEvent valueChangeEvent) {
        //find the review
        Review r = rs.findById(id);
        r.setIsApproved(!r.getIsApproved());
        rs.update(r);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
