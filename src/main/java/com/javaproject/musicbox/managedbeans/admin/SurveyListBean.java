package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Survey;
import com.javaproject.musicbox.services.SurveyService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class SurveyListBean {
    @Inject
    private SurveyService surveyDAO;

    private List<Survey> surveys;

    @PostConstruct
    private void init(){
        surveys = surveyDAO.findAll();
    }

    /**
     * Set a Survey as active and update list.
     * @param surveyId
     */
    public void activateSurvey(int surveyId){
        surveyDAO.setActiveSurvey(surveyId);
        //Update
        surveys = surveyDAO.findAll();
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }
}
