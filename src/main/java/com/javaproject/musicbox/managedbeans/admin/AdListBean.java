package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.entities.Ad;
import com.javaproject.musicbox.services.AdService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class AdListBean {
    @Inject
    private AdService adDAO;

    private List<Ad> ads;

    @PostConstruct
    private void init(){
        ads = adDAO.findAll();
    }

    /**
     * Calls the DAO method to activate an Ad and updates the page.
     * @param adID which AD to activate
     * @param position left(1) or right(2) ad
     */
    public void activateAd(int adID, int position){
        adDAO.activateAd(adID, position);
        //Update
        ads = adDAO.findAll();
    }

    public List<Ad> getAds() {
        return ads;
    }

    public void setAds(List<Ad> ads) {
        this.ads = ads;
    }
}
