package com.javaproject.musicbox.managedbeans.admin;

import com.javaproject.musicbox.services.AdService;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class AdEditorBean implements Serializable {
    @Inject
    private AdService adDAO;

    private String imageURL;
    private String websiteURL;

    /**
     * Creates a new Ad from manager input.
     * @return the outcome of the create action (redirect back to ad list page)
     */
    public String createNewAd(){
        //Call DAO create method
        adDAO.createAd(this.imageURL, this.websiteURL);

        //Redirect back to the ad list page
        return "/storefront/pages/admin/adList?faces-redirect=true";
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }
}
