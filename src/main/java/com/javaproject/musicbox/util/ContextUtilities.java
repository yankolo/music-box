package com.javaproject.musicbox.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;

public class ContextUtilities {
    /**
     * Helper method to redirect the user to the 404 page
     */
    public static void redirectTo404() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().responseSendError(404, "Page not found");
        context.responseComplete();
    }

    /**
     * Helper method to redirect to a certain page
     */
    public static void redirectTo(String page) throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect(externalContext.getRequestContextPath() + page);
    }
}