package com.javaproject.musicbox.util;

import com.javaproject.musicbox.entities.ShoppingCart;
import com.javaproject.musicbox.entities.User;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * Utility class for managing information stored in the session.
 */
public class SessionUtilities {
    /**
     * Get the Session Map
     *
     * @return The Session Map
     */
    public static Map<String, Object> getSessionMap() {
        return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
    }

    /**
     * Get the currently signed in (active session) user's id
     *
     * @return The user that is currently stored in the session
     */
    public static User getSessionUser() {
        Map<String, Object> sessionMap = getSessionMap();
        return (User) sessionMap.get("user");
    }

    /**
     * Sets the currently signed in user
     *
     * @param user The user to set in the session
     */
    public static void setSessionUser(User user) {
        //Set the user email attribute for other pages to identify the authenticated user
        Map<String, Object> sessionMap = getSessionMap();

        sessionMap.put("user", user);
    }

    /**
     * Removes the user from the session
     *
     */
    public static void removeSessionUser() {
        Map<String, Object> sessionMap = getSessionMap();

        sessionMap.put("user", null);
    }

    /**
     * Get the cart that is stored in the session
     * <p>
     * It should be noted that if the cart that is currently in the session,
     * the method initializes it since the cart should not be null
     * (it can be empty however of course)
     *
     * @return The shopping that is stored in the session
     */
    public static ShoppingCart getSessionCart() {
        Map<String, Object> sessionMap = getSessionMap();

        ShoppingCart sessionCart = (ShoppingCart) sessionMap.get("cart");

        // If the session cart doesn't not exist, create a new one
        if (sessionCart == null) {
            sessionCart = new ShoppingCart();
            sessionMap.put("cart", sessionCart);
        }

        return sessionCart;
    }

    /**
     * Logs out the current user by setting the User in the session map to null.
     */
    public static void logoutSessionUser(){
        Map<String, Object> sessionMap = getSessionMap();
        sessionMap.replace("user", null);
    }
}
