package com.javaproject.musicbox.util;

import com.javaproject.musicbox.entities.Item;
import com.javaproject.musicbox.entities.OrderItem;

import java.util.List;

/**
 * Utility class for calculating information that is related to purchases
 */
public class PurchaseUtilities {
    /**
     * Calculate the sum of item prices from a list of items
     *
     * @return The total price of the cart
     */
    public static double calcSumOfPrices(Item ... items) {
        double total = 0;

        for (Item item : items) {
            double itemPrice = 0;
            if (item.getSalePrice() == 0) {
                itemPrice = item.getListPrice();
            } else {
                itemPrice = item.getSalePrice();
            }

            total += itemPrice;
        }

        return total;
    }

    /**
     * Calculate the sum of item prices from a list of order items
     *
     * @return The total price of the cart
     */
    public static double calcSumOfPrices(OrderItem ... orderItems) {
        double total = 0;

        for (OrderItem orderItem : orderItems) {
            total += orderItem.getPrice();
        }

        return total;
    }

    /**
     * Calculate the taxes based on the subtotal
     * <p>
     * TODO remove hard coded tax rate
     *
     * @return The amount of taxes that will be applied to the cart subtotal
     */
    public static double calcTaxes(double subtotal) {
        return subtotal * 0.15;
    }

    /**
     * Calculate the grand total based on the subtotal and the taxes
     *
     * @return The grand total (i.e. subtotal and taxes combined)
     */
    public static double calcGrandTotal(double subtotal, double taxes) {
        return subtotal + taxes;
    }
}
