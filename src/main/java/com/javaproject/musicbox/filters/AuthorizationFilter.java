package com.javaproject.musicbox.filters;

import com.javaproject.musicbox.entities.User;

import javax.faces.application.ResourceHandler;
import javax.faces.context.FacesContext;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URLEncoder;

import static com.javaproject.musicbox.util.SessionUtilities.getSessionUser;

/**
 * Filter class for pages which cannot be accessed by unauthenticated users
 */
@WebFilter(filterName = "AuthFilter")
public class AuthorizationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Filter which web pages can be accessed only by logged-in users and set up redirects
     * See https://stackoverflow.com/questions/848010
     *
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        String requestURI = request.getRequestURI();
        String loginURL = request.getContextPath() + "/storefront/pages/public/login.xhtml";
        String registerURL = request.getContextPath() + "/storefront/pages/public/register.xhtml";
        String homeURL = request.getContextPath() + "/home.xhtml";
        String userLoggedInOnlyURL = request.getContextPath() + "/storefront/pages/authenticated/";
        String userLoggedInOnlyManager = request.getContextPath() + "/storefront/pages/admin/";

        boolean loggedIn = (session != null) && (session.getAttribute("user") != null);
        boolean loginRequest = request.getRequestURI().equals(loginURL);
        boolean registerRequest = request.getRequestURI().equals(registerURL);

        boolean restrictedRequest = request.getRequestURI().contains(userLoggedInOnlyURL) ||
                request.getRequestURI().contains(userLoggedInOnlyManager);

        String contextPath = request.getContextPath();

        //Getting the current session user
        User user = (User) request.getSession().getAttribute("user");

        //If the user is trying to access manager pages but is not a manager
        if(request.getRequestURI().contains(userLoggedInOnlyManager)){
            if(user != null) {
                if (!user.isManager()) {
                    response.sendRedirect(loginURL);
                }
            }
        }

        // Prevent the user from visiting the login/registration page if he is logged in
        if (loggedIn && (loginRequest || registerRequest)) {
            // Redirect to home
            response.sendRedirect(homeURL);
        }
        // If the user is not logged in and tries to access a restricted page
        // redirect to login
        else if (!loggedIn && restrictedRequest) {
            response.sendRedirect(loginURL);
        }
        // Allow everything else
        else {
            filterChain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
