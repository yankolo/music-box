package com.javaproject.musicbox.jobs;

import com.javaproject.musicbox.entities.Order;
import com.javaproject.musicbox.entities.OrderItem;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Stateless
public class EmailSender {
    @Resource(name = "mail/javamail")
    private Session mailSession;

    /**
     * This method sends a confirmation email to a client
     *
     * @param order The order that has been processed
     */
    @Asynchronous
    public void sendOrderConfirmation(Order order) throws MessagingException, UnsupportedEncodingException {
        int orderId = order.getOrderId();
        String clientEmail = order.getUser().getEmail();
        String clientFullName = order.getUser().getFirstName() + " " + order.getUser().getLastName();

        StringBuilder content = new StringBuilder();
        content.append("<!DOCTYPE html>");
        content.append("<html>");
        content.append("<body>");;
        content.append(String.format("<h3>Hi %s,</h3>", clientFullName));
        content.append(String.format("<p>This is a confirmation email. Your order (#%d) ", orderId));
        content.append("has been successfully processed. You can view your previous purchases ");
        content.append("by visiting \"My Orders\" in \"Your Account\" ");
        content.append("(If you have purchased any songs, you'll be able to download them there also).</p>");
        content.append("<p>Here are the items you have bought:</p>");
        content.append("<ul>");
        for (OrderItem orderItem : order.getOrderItems()) {
            String name = orderItem.getItem().getName();
            String type = orderItem.getItem().getItemType();
            double price = orderItem.getPrice();
            content.append(String.format("<li>%s (%s) - $%.2f</li>", name, type, price));
        }
        content.append("</ul>");
        content.append(String.format("<span>Subtotal - $%.2f</span><br>", order.getSubtotal()));
        content.append(String.format("<span>Taxes - $%.2f</span><br>", order.getTaxes()));
        content.append(String.format("<span>Grand Total - $%.2f</span><br>", order.getGrandTotal()));
        content.append("<p>Thank you for shopping at MusicBox!</p>");
        content.append("</body>");
        content.append("</html>");

        Message message = new MimeMessage(mailSession);

        message.setSubject(String.format("MusicBox - Your Order (#%d) Is Complete", orderId));
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(clientEmail, clientFullName));
        message.setContent(content.toString(), "text/html; charset=utf-8");
        Transport.send(message);
    }
}
