package com.javaproject.musicbox.eventsdata;

import com.javaproject.musicbox.entities.User;

public class LoginEventData {
    private User loggedInUser;

    public LoginEventData(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}
