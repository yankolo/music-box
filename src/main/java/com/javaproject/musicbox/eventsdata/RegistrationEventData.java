package com.javaproject.musicbox.eventsdata;

import com.javaproject.musicbox.entities.User;

public class RegistrationEventData {
    private User registeredUser;

    public RegistrationEventData(User registeredUser) {
        this.registeredUser = registeredUser;
    }

    public User getRegisteredUser() {
        return registeredUser;
    }

    public void setRegisteredUser(User registeredUser) {
        this.registeredUser = registeredUser;
    }
}
