package com.javaproject.musicbox.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("creditCardValidator")
public class CreditCardValidator implements Validator {
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException
    {
        String ccNumber = (String) value;

        // Remove all spaces from credit card number in case there are some
        ccNumber = ccNumber.replaceAll(" ","");

        // Algorithm taken from
        // https://www.journaldev.com/1443/java-credit-card-validation-luhn-algorithm-java

        int[] ints = new int[ccNumber.length()];
        for (int i = 0; i < ccNumber.length(); i++) {
            ints[i] = Integer.parseInt(ccNumber.substring(i, i + 1));
        }
        for (int i = ints.length - 2; i >= 0; i = i - 2) {
            int j = ints[i];
            j = j * 2;
            if (j > 9) {
                j = j % 10 + 1;
            }
            ints[i] = j;
        }
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
        }
        if (sum % 10 != 0) {
            throw new ValidatorException(new FacesMessage("Credit Card Number Is Invalid"));
        }
    }

}
