document.addEventListener('DOMContentLoaded',
    function loadCreditCard() {
        var card = new Card({
            // a selector or DOM element for the form where users will
            // be entering their information
            form: '.js-checkout-form', // *required*
            // a selector or DOM element for the container
            // where you want the card to appear
            container: '.js-checkout-cc-card', // *required*

            formSelectors: {
                numberInput: '.js-cc-number', // optional — default input[name="number"]
                expiryInput: '.js-cc-exp', // optional — default input[name="expiry"]
                cvcInput: '.js-cc-cvv', // optional — default input[name="cvc"]
                nameInput: '.js-cc-name' // optional - defaults input[name="name"]
            },

            width: 350, // optional — default 350px
            formatting: true, // optional - default true

            // Strings for translation - optional
            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },

            // Default placeholders for rendered fields - optional
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'Full Name',
                expiry: '••/••',
                cvc: '•••'
            },

            // if true, will log helpful messages for setting up Card
            debug: false // optional - default false
        });
    }, false);