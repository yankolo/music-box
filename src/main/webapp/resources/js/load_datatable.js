function loadDatatable() {
    $(".dataTable").DataTable({

        // To prevent the display of a "normal" HTML table
        // the table is hidden at first and then shown
        // after it is initialized
        "initComplete": function (settings, json) {
            $(".js-dataTable-wrapper").css("display", "block");
        }
    });
}
function loadNoPageDatatable() {
        $(".dataTableNoPage").DataTable({
            // To prevent the display of a "normal" HTML table
            // the table is hidden at first and then shown
            // after it is initialized
            "initComplete": function (settings, json) {
                $(".js-dataTable-wrapper").css("display", "block");
            },

            paging: false,
            scrollY: 300,
            bInfo: false
        });
}

$(document).ready(function(){
   loadDatatable();
   loadNoPageDatatable();
});

