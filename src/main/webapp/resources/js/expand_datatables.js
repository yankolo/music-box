/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function click(){
    $('#plus_expand_genre').off();
    $('#plus_expand_artist_track').off();
    $('#plus_expand_album_track').off();
    $('#plus_expand_artist_album').off();
    
    $('#plus_expand_genre').click(function(){
        $('#plus_expand_genre').toggleClass('arrow_up');
        $('#genre_table').toggle();
    });
    
    $('#plus_expand_artist_track').click(function(){
        $('#plus_expand_artist_track').toggleClass('arrow_up');
        $('#artist_track_table').toggle();
    });
    
    $('#plus_expand_album_track').click(function(){
        $('#plus_expand_album_track').toggleClass('arrow_up');
        $('#album_track_table').toggle();
    });
    
    $('#plus_expand_artist_album').click(function(){
        $('#plus_expand_artist_album').toggleClass('arrow_up');
        $('#artist_album_table').toggle();
    });
}