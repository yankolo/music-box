<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <!-- Indicates what version of the object model this POM is using. Always 4.0.0 -->
    <modelVersion>4.0.0</modelVersion>

    <!-- Project's GAV -->
    <groupId>com.javaproject</groupId>
    <artifactId>musicbox</artifactId>
    <version>1.0-FINAL</version>

    <!-- Package type to be used by this artifact -->
    <packaging>war</packaging>

    <!-- The display name used for the project. -->
    <name>MusicBox</name>

    <properties>
        <!-- The character encoding scheme that will be used for the reading and writing of files. Good practice to define it -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- Specifies the version of source code accepted -->
        <maven.compiler.source>1.8</maven.compiler.source>

        <!-- Specifies the version to of the VM that will be targeted. A lower version VM cannot execute package -->
        <maven.compiler.target>1.8</maven.compiler.target>
        
    </properties>

    <dependencies>
        <!-- The project uses the JPA API for database connectivity. To use it, it is needed to add a JPA implementation like EclipseLink -->
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>eclipselink</artifactId>
            <version>2.7.3</version>
        </dependency>

        <!-- Enable the creation of Java EE web application -->
        <dependency>
            <groupId>javax</groupId>
            <artifactId>javaee-web-api</artifactId>
            <version>8.0</version>
            <scope>provided</scope>
        </dependency>

        <!-- Dependency for JavaMail -->
        <dependency>
            <groupId>javax.mail</groupId>
            <artifactId>javax.mail-api</artifactId>
            <version>1.6.2</version>
        </dependency>

        <!-- A MySQL database is used. This dependency adds the necessary JDBC driver -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.13</version>
        </dependency>

        <!-- JUnit4 test framework -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.rometools</groupId>
            <artifactId>rome</artifactId>
            <version>1.12.0</version>
        </dependency>
        
        <dependency>
            <groupId>org.primefaces</groupId>
            <artifactId>primefaces</artifactId>
            <version>6.2</version>
        </dependency>

        <!-- Utility library for JSF -->
        <dependency>
            <groupId>org.omnifaces</groupId>
            <artifactId>omnifaces</artifactId>
            <version>3.2</version>
        </dependency>

        <dependency>
            <groupId>org.jboss.arquillian.container</groupId>
            <artifactId>arquillian-glassfish-remote-3.1</artifactId>
            <version>1.0.2</version>
            <scope>test</scope>
        </dependency>
        <!-- Resolves dependencies from the pom.xml when explicitly referred to
        in the Arquillian deploy method -->
        <dependency>
            <groupId>org.jboss.shrinkwrap.resolver</groupId>
            <artifactId>shrinkwrap-resolver-depchain</artifactId>
            <type>pom</type>
            <scope>test</scope>
        </dependency>
        <!-- Connects Arquillian to JUnit -->
        <dependency>
            <groupId>org.jboss.arquillian.junit</groupId>
            <artifactId>arquillian-junit-container</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>3.11.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.jboss.arquillian.extension</groupId>
            <artifactId>arquillian-persistence-dbunit</artifactId>
            <version>1.0.0.Alpha7</version>
        </dependency>
        
        <!-- Selenium dependencies -->
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>htmlunit-driver</artifactId>
            <version>2.33.3</version>
            <scope>test</scope>
        </dependency>

        <!-- You will need a driver dependency for every browser you use in
        testing. You can find the maven dependencies at
        https://mvnrepository.com/artifact/org.seleniumhq.selenium -->
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-chrome-driver</artifactId>
            <version>3.141.0</version>
            <scope>test</scope>
        </dependency>

        <!-- Normally you must download an exe for each browser. This library
        will retrieve the the necessary file and place it in the classpath for
        selenium to use -->
        <dependency>
            <groupId>io.github.bonigarcia</groupId>
            <artifactId>webdrivermanager</artifactId>
            <version>3.4.0</version>
            <scope>test</scope>
        </dependency>

        <!-- January 2018 Discovered that there is a guava conflict that 
        prevents Selenium from functioning unless this explicit dependency is 
        included for testing -->
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>22.0</version>
            <scope>test</scope>
        </dependency>

        <!-- add slf4j interfaces to classpath -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.6.4</version>
            <scope>compile</scope>
        </dependency>

        <!-- add commons logging to slf4j bridge to classpath -->
        <!-- acts as jcl but routes commons-logging calls to slf4j -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>1.6.4</version>
            <scope>runtime</scope>
        </dependency>

        <!-- add log4j binding to classpath -->
        <!-- routes slf4j calls to log4j -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>1.6.4</version>
            <scope>runtime</scope>
        </dependency>

        <!-- add log4j to classpath -->
        <!-- does the logging -->
        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
            <version>1.2.16</version>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.jboss.arquillian</groupId>
                <artifactId>arquillian-bom</artifactId>
                <version>1.2.0.Final</version>
                <scope>import</scope>
                <type>pom</type>
            </dependency>
            <dependency>
                <groupId>org.jboss.shrinkwrap.resolver</groupId>
                <artifactId>shrinkwrap-resolver-bom</artifactId>
                <version>3.1.2</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    
    <build>
        <plugins>
            <!-- Sass compiler -->
            <plugin>
                <groupId>nl.geodienstencentrum.maven</groupId>
                <artifactId>sass-maven-plugin</artifactId>
                <version>2.25</version>
                <executions>
                    <execution>
                        <id>build</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>update-stylesheets</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <sassSourceDirectory>
                        ${project.basedir}/src/main/webapp/resources/scss
                    </sassSourceDirectory>
                    <destination>
                        ${project.build.directory}/${project.build.finalName}/resources/css
                    </destination>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <testFailureIgnore>true</testFailureIgnore>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>